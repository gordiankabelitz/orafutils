// stl includes
#include <stdlib.h>
#include <iostream>
#include <string>
#include <time.h>
#include <chrono>

// itk includes
#include <itkImage.h>
#include <itkImageRegionIteratorWithIndex.h>
#include <itkImageRegionIterator.h>
#include <itkImageFileWriter.h>
#include <itkEuler3DTransform.h>
#include <itkExtractImageFilter.h>
#include <itkMath.h>

#include "CommonToolsNew.hxx"

// ora includes
#include "oraRTKDRRFilter.h"
#include "oraCudaDRRFilter.h"
#include "oraProjectionGeometry.h"
#include "oraIntensityTransferFunction.h"
#include "oraCPUSiddonDRRFilter.h"
#include "oraDRRFilter.h"

// rtk includes
#include "rtkCudaForwardProjectionImageFilter.h"

// quite large-minded tolerance for DRR pixel intensity outcome comparison
// (on-the-fly vs. off-the-fly ITF mapping); "large-minded" comes from the fact
// that DRR intensities are essentially a sum and internally we use FLOAT for
// persistent ITF mapping (vs. DOUBLE precision)
#define ACCURACY 100.f

/**
* Tests base functionality of:
*
*   ora::RTKDRRFilter
*   ora::CPUSiddonDRRFilter
*
* Test application result is 0 (EXIT_SUCCESS) if SUCCESSFUL.
*
* Arguments: run test with -h or --help option to explore them!
*
* @see oraRTKDRRFilter.h
* @see oraRTKDRRFilter.txx
*
* @author kabelitz
* @version 1.0
*
* \ingroup Tests
*/

typedef float VolumePixelType;
typedef float PixelType;
typedef itk::Euler3DTransform<double> TransformType;
typedef ora::ProjectionGeometry ProjectionGeometryType;
typedef ProjectionGeometryType::Pointer ProjectionGeometryPointer;

typedef ora::RTKDRRFilter<VolumePixelType, PixelType>                 CudaDRRFilterType;
typedef CudaDRRFilterType::InputImageType                             CudaVolumeImageType;
typedef CudaDRRFilterType::OutputImageType                            CudaDRRImageType;
typedef itk::CudaImage<PixelType, 2>                                  CudaDRR2DImageType;
typedef itk::ExtractImageFilter<CudaDRRImageType, CudaDRR2DImageType> CudaExtractorType;
typedef itk::ImageRegionIteratorWithIndex<CudaVolumeImageType>        CudaVolumeIteratorType;
typedef itk::ImageFileWriter<CudaVolumeImageType>                     CudaVolumeWriterType;
typedef itk::ImageFileWriter<CudaDRRImageType>                        CudaDRRWriterType;
typedef itk::ImageFileWriter<CudaDRR2DImageType>                      CudaDRR2DWriterType;
typedef itk::ImageRegionIterator<CudaDRRImageType>                    CudaIntensityIteratorType;
typedef itk::ImageRegionIterator<CudaDRRFilterType::MaskImageType>    CudaMaskIntensityIteratorType;
typedef ora::IntensityTransferFunction<PixelType, PixelType>::Pointer ITFPointer;

typedef ora::CPUSiddonDRRFilter<VolumePixelType, PixelType>           DRRFilterType;
typedef DRRFilterType::InputImageType                                 VolumeImageType;
typedef DRRFilterType::OutputImageType                                DRRImageType;
typedef itk::Image<PixelType, 2>                                      DRR2DImageType;
typedef itk::ExtractImageFilter<DRRImageType, DRR2DImageType>         ExtractorType;
typedef itk::ImageRegionIteratorWithIndex<VolumeImageType>            VolumeIteratorType;
typedef itk::ImageFileWriter<VolumeImageType>                         VolumeWriterType;
typedef itk::ImageFileWriter<DRRImageType>                            DRRWriterType;
typedef itk::ImageFileWriter<DRR2DImageType>                          DRR2DWriterType;
typedef itk::ImageRegionIterator<DRRImageType>                        IntensityIteratorType;
typedef itk::ImageRegionIterator<DRRFilterType::MaskImageType>        MaskIntensityIteratorType;

typedef rtk::CudaForwardProjectionImageFilter<CudaVolumeImageType, CudaVolumeImageType> CudaForwardProjectionFilterType;
typedef CudaForwardProjectionFilterType::Pointer                                        CudaProjectorPointer;
typedef rtk::ThreeDCircularProjectionGeometry                                           RTKProjectionGeometryType;
typedef RTKProjectionGeometryType::Pointer                                              RTKGeometryPointer;

typedef std::chrono::high_resolution_clock     Clock;
typedef std::chrono::milliseconds              msType;
typedef std::chrono::time_point<Clock, double> TimePoint;

int oraEvaluateDRRFilterTest(int argc, char *argv[])
{
	// basic command line pre-processing:
	auto last = 0;
	bool Verbose = false;
	bool ImageOutput = false;
	for (auto i = 1; i < argc; i++)
	{
		if (std::string(argv[i]) == "-v" || std::string(argv[i]) == "--verbose")
		{
			Verbose = true;
			last = i;
			continue;
		}
		if (std::string(argv[i]) == "-io" || std::string(argv[i]) == "--image-output")
		{
			ImageOutput = true;
			last = i;
			continue;
		}
		if (std::string(argv[i]) == "-h" || std::string(argv[i]) == "--help")
		{
			std::cout << "No help information needed.\n"
				<< "USAGE: oraEvaluateDRRFilterTest [options]" << std::endl;
			return EXIT_SUCCESS;
		}
	}

	auto ok = true; // OK-flag for whole test
	bool lok; // local OK-flag for a test sub-section
	bool culok; // local OK-flag for a test cuda sub-section
	lok = true; // initialize sub-section's success state
	culok = true;


	VERBOSE(Verbose, << "  * Generating input data ...\n");
	// DRRFilter using CUDA
	auto CudaDRRFilter = CudaDRRFilterType::New();
	CudaDRRFilter->SetNumberOfWorkUnits(1);

	// Reference CudaImageFilter
	auto RTKProjector = CudaForwardProjectionFilterType::New();
	RTKProjector->InPlaceOff();

	// DRRFilter using CPU
	auto DRRFilter = DRRFilterType::New();
	CudaDRRFilter->SetNumberOfWorkUnits(1);

	// Create volumes for projection
	int size[] = {80,70,30};
	int index[] = { 0,0,0 };
	float spacing[] = {0.7f,0.8f,1.8f};
	float origin[] = { -(size[0] * spacing[0]) / 2.f, -(size[1] * spacing[1]) / 2.f, -(size[2] * spacing[2]) / 2.f }; // centered

	VolumeImageType::SizeType isize;
	VolumeImageType::IndexType iindex;
	VolumeImageType::SpacingType ispacing;
	VolumeImageType::PointType iorigin;
	auto volume = VolumeImageType::New();
	
	CudaVolumeImageType::SizeType cusize;
	CudaVolumeImageType::IndexType cuindex;
	CudaVolumeImageType::SpacingType cuspacing;
	CudaVolumeImageType::PointType cuorigin;
	auto cuvolume = CudaVolumeImageType::New();
	auto cuvolume2 = CudaVolumeImageType::New();

	for(auto i=0; i < 3; ++i)
	{
		isize[i] = size[i];
		iindex[i] = index[i];
		ispacing[i] = spacing[i];
		iorigin[i] = origin[i];
		
		cusize[i] = size[i];
		cuindex[i] = index[i];
		cuspacing[i] = spacing[i];
		cuorigin[i] = origin[i];
	}

	// direction
	VolumeImageType::DirectionType idirection;
	idirection.SetIdentity();
	CudaVolumeImageType::DirectionType cudirection;
	cudirection.SetIdentity();
	// region
	VolumeImageType::RegionType iregion;
	iregion.SetIndex(iindex);
	iregion.SetSize(isize);
	volume->SetRegions(iregion);
	volume->SetSpacing(ispacing);
	volume->SetOrigin(iorigin);
	volume->Allocate();

	CudaVolumeImageType::RegionType curegion;
	curegion.SetIndex(cuindex);
	curegion.SetSize(cusize);
	cuvolume->SetRegions(curegion);
	cuvolume->SetSpacing(cuspacing);
	cuvolume->SetOrigin(cuorigin);
	cuvolume->Allocate();

	CudaVolumeImageType::RegionType curegion2;
	curegion2.SetIndex(cuindex);
	curegion2.SetSize(cusize);
	cuvolume2->SetRegions(curegion2);
	cuvolume2->SetSpacing(cuspacing);
	cuvolume2->SetOrigin(cuorigin);
	cuvolume2->Allocate();

	// fill volume with random values
	VolumePixelType v = 0;
	VolumeIteratorType it(volume, iregion);
	VolumeImageType::IndexType idx;
	CudaVolumeIteratorType cuit(cuvolume, curegion);
	CudaVolumeIteratorType cuit2(cuvolume2, curegion2);

	srand(time(nullptr));
	for(it.GoToBegin(), cuit.GoToBegin(), cuit2.GoToBegin(); !it.IsAtEnd(); ++it, ++cuit, ++cuit2)
	{
		idx = it.GetIndex();
		if(idx[0] > 10 && idx[0] < 70 && idx[1] > 5 && idx[1] < 65 && idx[2] > 2 && idx[2] < 28)
		{
			v = rand() % 1000 + 1000; // tissue
		}
		else
		{
			v = 0; // air
		}
		it.Set(v);
		cuit.Set(v);
		cuit2.Set(v);
	}

	// update cuda volume to be sure that non of the buffers are dirty
	cuvolume->UpdateBuffers();
	cuvolume2->UpdateBuffers();

	if (ImageOutput)
	{
		auto w = VolumeWriterType::New();
		w->SetInput(volume);
		w->SetFileName("volume.mhd");
		try
		{
			w->Update();
		}
		catch (itk::ExceptionObject &EO)
		{
			lok = false;
			EO.Print(std::cout);
		}
		w = ITK_NULLPTR;

		auto cuw = CudaVolumeWriterType::New();
		cuw->SetInput(cuvolume);
		cuw->SetFileName("cuvolume.mhd");
		try
		{
			cuw->Update();
		}
		catch (itk::ExceptionObject &EO)
		{
			culok = false;
			EO.Print(std::cout);
		}
		cuw = ITK_NULLPTR;

		auto cuw2 = VolumeWriterType::New();
		cuw2->SetInput(cuvolume2);
		cuw2->SetFileName("cuvolume2.mhd");
		try
		{
			cuw2->Update();
		}
		catch (itk::ExceptionObject &EO)
		{
			culok = false;
			EO.Print(std::cout);
		}
		cuw2 = ITK_NULLPTR;
	}
	ok = ok && lok && culok;
	VERBOSE(Verbose, << "CudaVolume: " << (culok ? "OK" : "FAILURE") << "\n");
	VERBOSE(Verbose, << "Volume: " << (lok ? "OK" : "FAILURE") << "\n");

	VERBOSE(Verbose, << "  * Generating and applying DRR properties ...\n");
	culok = true;
	lok = true;

	// Generating Projection Geometry
	auto ProjectionGeometry = ProjectionGeometryType::New();
	double detectorRowOrientation[3];
	detectorRowOrientation[0] = 1;
	detectorRowOrientation[1] = 0;
	detectorRowOrientation[2] = 0;
	double detectorColumnOrientation[3];
	detectorColumnOrientation[0] = 0;
	detectorColumnOrientation[1] = 1;
	detectorColumnOrientation[2] = 0;
	auto thelp = TransformType::New();
	TransformType::ParametersType parameters(6);
	parameters.Fill(0);
	// initial rotation in radians
	parameters[0] = 1;
	parameters[1] = 0.03;
	parameters[2] = -0.035;
	thelp->SetParameters(parameters);
	TransformType::InputPointType p;
	p[0] = detectorRowOrientation[0];
	p[1] = detectorRowOrientation[1];
	p[2] = detectorRowOrientation[2];
	p = thelp->TransformPoint(p);
	detectorRowOrientation[0] = p[0];
	detectorRowOrientation[1] = p[1];
	detectorRowOrientation[2] = p[2];
	p[0] = detectorColumnOrientation[0];
	p[1] = detectorColumnOrientation[1];
	p[2] = detectorColumnOrientation[2];
	p = thelp->TransformPoint(p);
	detectorColumnOrientation[0] = p[0];
	detectorColumnOrientation[1] = p[1];
	detectorColumnOrientation[2] = p[2];
	thelp = nullptr;
	ProjectionGeometry->SetDetectorRowOrientation(detectorRowOrientation);
	ProjectionGeometry->SetDetectorColumnOrientation(detectorColumnOrientation);
	// set drr Origin
	double drrOrigin[3];
	drrOrigin[0] = -100;
	drrOrigin[1] = -80;
	drrOrigin[2] = -150;
	ProjectionGeometry->SetDetectorOrigin(drrOrigin);
	// set drr Detector size
	int drrSize[2];
	drrSize[0] = 200;
	drrSize[1] = 160;
	ProjectionGeometry->SetDetectorSize(drrSize);
	// set drr spacing
	double drrSpacing[2];
	drrSpacing[0] = 1.0;
	drrSpacing[1] = 1.0;
	ProjectionGeometry->SetDetectorPixelSpacing(drrSpacing);
	// set source position
	double drrFocalSpot[3];
	drrFocalSpot[0] = 100;
	drrFocalSpot[1] = 10;
	drrFocalSpot[2] = 800;
	ProjectionGeometry->SetSourcePosition(drrFocalSpot);

	DRRFilter->SetCurrentDRROutputIndex(0);
	DRRFilter->SetProjectionGeometry(0, ProjectionGeometry);

	CudaDRRFilter->SetCurrentDRROutputIndex(0);
	CudaDRRFilter->SetGeometry(0, ProjectionGeometry); // ATTENTION: CudaDRRFilter needs SetGeometry() instead of SetProjectionGeometry()

	// Change projection geometry for rtk
	auto RTKGeometry = RTKProjectionGeometryType::New();
	rtk::ThreeDCircularProjectionGeometry::PointType srcPt;
	rtk::ThreeDCircularProjectionGeometry::PointType detPt;
	rtk::ThreeDCircularProjectionGeometry::VectorType rowVec;
	rtk::ThreeDCircularProjectionGeometry::VectorType colVec;
	for (auto i = 0; i<3; ++i)
	{
		srcPt[i]  = drrFocalSpot[i];
		detPt[i]  = drrOrigin[i];
		rowVec[i] = detectorRowOrientation[i];
		colVec[i] = detectorColumnOrientation[i];
	}
	RTKGeometry->AddProjection(srcPt, detPt, rowVec, colVec);
	RTKProjector->SetGeometry(RTKGeometry);

	ProjectionGeometry->IsGeometryValid();
	ProjectionGeometry->Print(std::cout);

	// set volume transform to identity
	auto VolumeTransform = TransformType::New();
	VolumeTransform->SetIdentity();
	DRRFilter->SetTransform(VolumeTransform);
	CudaDRRFilter->SetTransform(VolumeTransform);

	CudaDRRFilter->SetInput(cuvolume);
	DRRFilter->SetInput(volume);
	RTKProjector->SetInput(1, cuvolume2);

	ok = ok && lok && culok;
	VERBOSE(Verbose, << "CudaDRRFilter: " << (culok ? "OK" : "FAILURE") << "\n");
	VERBOSE(Verbose, << "DRRFilter: " << (lok ? "OK" : "FAILURE") << "\n");

	VERBOSE(Verbose, << "  * Computing DRRs (3D and 2D casting) ...\n");
	culok = true;
	lok = true;

	for (unsigned int i = 1; i < 5; i++)
	{
		if (i > 0)
		{
			TransformType::ParametersType pars(6);
			pars.Fill(0);
			pars[0] += static_cast<double>(rand() % 101 - 50) / 400.;
			pars[1] += static_cast<double>(rand() % 101 - 50) / 400.;
			pars[2] += static_cast<double>(rand() % 101 - 50) / 400.;
			pars[3] += static_cast<double>(rand() % 101 - 50) / 5.;
			pars[4] += static_cast<double>(rand() % 101 - 50) / 5.;
			pars[5] += static_cast<double>(rand() % 101 - 50) / 5.;
			VolumeTransform->SetParameters(pars);
		}

		// Run DRRFilter
		auto SiddonExtractor = ExtractorType::New();
		SiddonExtractor->SetDirectionCollapseToIdentity();
		SiddonExtractor->SetInput(DRRFilter->GetOutput());
		try
		{
			DRRFilter->Update();
			if (DRRFilter->GetOutput())
			{
				if (ImageOutput)
				{
					auto w = DRRWriterType::New();
					char buff[100];
					sprintf_s(buff, "drr3D_Siddon_%d.mhd", i);
					w->SetFileName(buff);
					w->SetInput(DRRFilter->GetOutput());
					try
					{
						w->Update();
					}
					catch (...)
					{
						lok = false;
					}
					w = nullptr;
				}

				// Casting to 2d output
				auto DRRRegion = DRRFilter->GetOutput()->GetLargestPossibleRegion();
				auto ESize = DRRRegion.GetSize();
				ESize[2] = 0; // 3D->2D
				auto eindex = DRRRegion.GetIndex();
				eindex[2] = 0; // 1st and only slice
				DRRImageType::RegionType ExtractRegion;
				ExtractRegion.SetIndex(eindex);
				ExtractRegion.SetSize(ESize);
				SiddonExtractor->SetDirectionCollapseToIdentity();
				SiddonExtractor->SetExtractionRegion(ExtractRegion);
				SiddonExtractor->Update();

				if (ImageOutput)
				{
					auto w2d = DRR2DWriterType::New();
					char buff[100];
					sprintf_s(buff, "drr2D_Siddon_%d.mhd", i);
					w2d->SetFileName(buff);
					w2d->SetInput(SiddonExtractor->GetOutput());
					try
					{
						w2d->Update();
					}
					catch (...)
					{
						lok = false;
					}
					w2d = nullptr;
				}
			}
			else
			{
				lok = false;
			}
		}
		catch (...)
		{
			lok = false;
		}

		// Run CudaDRRFilter
		auto CudaExtractor = CudaExtractorType::New();
		CudaExtractor->SetDirectionCollapseToIdentity();
		CudaExtractor->SetInput(CudaDRRFilter->GetOutput());
		try
		{
			CudaDRRFilter->Update();
			if (CudaDRRFilter->GetOutput())
			{
				if (ImageOutput)
				{
					auto cuw = DRRWriterType::New();
					char buff[100];
					sprintf_s(buff, "drr3D_Cuda_%d.mhd", i);
					cuw->SetFileName(buff);
					cuw->SetInput(CudaDRRFilter->GetOutput());
					try
					{
						cuw->Update();
					}
					catch (...)
					{
						culok = false;
					}
					cuw = nullptr;
				}

				// Casting to 2D output
				auto CudaRegion = CudaDRRFilter->GetOutput()->GetLargestPossibleRegion();
				auto CudaSize = CudaRegion.GetSize();
				CudaSize[2] = 0;
				auto CudaIndex = CudaRegion.GetIndex();
				CudaIndex[2] = 0;
				CudaVolumeImageType::RegionType CudaExtractRegion;
				CudaExtractRegion.SetIndex(CudaIndex);
				CudaExtractRegion.SetSize(CudaSize);
				CudaExtractor->SetExtractionRegion(CudaExtractRegion);
				CudaExtractor->Update();

				if (ImageOutput)
				{
					auto cu2dw = CudaDRR2DWriterType::New();
					char buff[100];
					sprintf_s(buff, "drr2D_Cuda_%d.mhd", i);
					cu2dw->SetFileName(buff);
					cu2dw->SetInput(CudaExtractor->GetOutput());
					try
					{
						cu2dw->Update();
					}
					catch (...)
					{
						culok = false;
					}
					cu2dw = nullptr;
				}
			}
			else
			{
				culok = false;
			}
		}
		catch (...)
		{
			culok = false;
		}

		// Run RTK Cuda reference filter
		// RTK Cuda added an image overlay and cannot be used together with CudaDDRFilter
		{
			auto RTKProjectionStack = CudaVolumeImageType::New();
			CudaVolumeImageType::IndexType rtkidx;
			rtkidx[0] = 0;
			rtkidx[1] = 0;
			rtkidx[2] = 0;
			CudaVolumeImageType::SizeType rtksize;
			rtksize[0] = drrSize[0];
			rtksize[1] = drrSize[1];
			rtksize[2] = 1;
			CudaVolumeImageType::RegionType rtkregion(rtkidx, rtksize);
			RTKProjectionStack->SetRegions(rtkregion);
			RTKProjectionStack->Allocate();
			CudaVolumeImageType::SpacingType rtkspacing;
			rtkspacing[0] = drrSpacing[0];
			rtkspacing[1] = drrSpacing[1];
			rtkspacing[2] = 1;
			RTKProjectionStack->SetSpacing(rtkspacing);
			CudaVolumeImageType::PointType rtkorigin;
			rtkorigin[0] = 0;
			rtkorigin[1] = 0;
			rtkorigin[2] = 0;
			RTKProjectionStack->SetOrigin(rtkorigin);
			RTKProjector->SetInput(0, RTKProjectionStack);

			auto RTKExtractor = CudaExtractorType::New();
			RTKExtractor->SetDirectionCollapseToIdentity();
			RTKExtractor->SetInput(RTKProjector->GetOutput());
			if (false)
			{
				try
				{
					RTKProjector->Update();
					if (RTKProjector->GetOutput())
					{
						if (ImageOutput)
						{
							typedef itk::ImageFileWriter<CudaVolumeImageType> FileWriterType;
							auto rtkw = FileWriterType::New();
							rtkw->SetInput(RTKProjector->GetOutput());
							char buff[100];
							sprintf_s(buff, "drr3D_RTK_%d.mhd", i);
							rtkw->SetFileName(buff);

							try
							{
								rtkw->Update();
							}
							catch (itk::ExceptionObject err)
							{
								std::cout << "Exception caught!" << std::endl;
								std::cout << err << std::endl;
							}
						}

						// Casting to 2D output
						auto RTKRegion = RTKProjector->GetOutput()->GetLargestPossibleRegion();
						auto RTKSize = RTKRegion.GetSize();
						RTKSize[2] = 0;
						auto RTKIndex = RTKRegion.GetIndex();
						RTKIndex[2] = 0;
						CudaVolumeImageType::RegionType RTKExtractRegion;
						RTKExtractRegion.SetIndex(RTKIndex);
						RTKExtractRegion.SetSize(RTKSize);
						RTKExtractor->SetExtractionRegion(RTKExtractRegion);
						RTKExtractor->Update();

						if (ImageOutput)
						{
							auto rtk2dw = CudaDRR2DWriterType::New();
							char buff[100];
							sprintf_s(buff, "drr2D_RTK_%d.mhd", i);
							rtk2dw->SetFileName(buff);
							rtk2dw->SetInput(RTKExtractor->GetOutput());
							try
							{
								rtk2dw->Update();
							}
							catch (...)
							{
								culok = false;
							}
							rtk2dw = nullptr;
						}
					}
				}
				catch (itk::ExceptionObject EO)
				{
					std::cout << "RTK error." << std::endl;
				}
			}
		}
	}

	ok = ok && lok && culok;
	VERBOSE(Verbose, << "CudaDRRFilter: " << (culok ? "OK" : "FAILURE") << "\n");
	VERBOSE(Verbose, << "DRRFilter: " << (lok ? "OK" : "FAILURE") << "\n");


	VERBOSE(Verbose, << "  * Independent DRR output checks ...\n");
	lok = true;
	culok = true;

	// Siddon DRR Testing
	DRRFilter->SetNumberOfIndependentOutputs(3); 
	DRRFilter->SetCurrentDRROutputIndex(0);
	if (!DRRFilter->GetOutput(0) || !DRRFilter->GetOutput(1) || !DRRFilter->GetOutput(2))
		lok = false;	

	// largest possible region of the un-computed outputs must be 0!
	DRRImageType::RegionType tregion;
	tregion = DRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() <= 0)
		lok = false;
	tregion = DRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() != 0)
		lok = false;
	tregion = DRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() != 0)
		lok = false;
	DRRFilter->SetCurrentDRROutputIndex(1); // compute 2nd output
	DRRFilter->SetProjectionGeometry(1, ProjectionGeometry);
	try
	{
		DRRFilter->Update();
	}
	catch (...)
	{
		lok = false;
	}

	if (!DRRFilter->GetOutput(0) || !DRRFilter->GetOutput(1) || !DRRFilter->GetOutput(2))
		lok = false;
	tregion = DRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() <= 0)
		lok = false;
	tregion = DRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() <= 0)
		lok = false;
	tregion = DRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() != 0)
		lok = false;
	DRRFilter->SetCurrentDRROutputIndex(2); // compute 3rd output
	DRRFilter->SetProjectionGeometry(2, ProjectionGeometry);
	try
	{
		DRRFilter->Update();
	}
	catch (...)
	{
		lok = false;
	}

	if (!DRRFilter->GetOutput(0) || !DRRFilter->GetOutput(1) || !DRRFilter->GetOutput(2))
		lok = false;
	tregion = DRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() <= 0)
		lok = false;
	tregion = DRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() <= 0)
		lok = false;
	tregion = DRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	if (tregion.GetNumberOfPixels() <= 0)
		lok = false;

	// now check whether the outputs are REALLY independent from each other:
	DRRFilter->SetNumberOfIndependentOutputs(3); // 3 independent outputs again
	DRRFilter->SetCurrentDRROutputIndex(0); // compute first input (again)
	DRRFilter->SetProjectionGeometry(1, ProjectionGeometry); //resetting projection geometries
	DRRFilter->SetProjectionGeometry(2, ProjectionGeometry);
	try
	{
		DRRFilter->Update();
	}
	catch (...)
	{
		lok = false;
	}
	// store image intensities in a reference array:
	tregion = DRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	int refArr1Size = tregion.GetNumberOfPixels();
	auto *refArr1 = new PixelType[refArr1Size];
	IntensityIteratorType dit1(DRRFilter->GetOutput(0), tregion);
	auto c = 0;
	for (dit1.GoToBegin(); !dit1.IsAtEnd(); ++dit1)
		refArr1[c++] = dit1.Get();
	// change some DRR-props:
	DRRFilter->SetCurrentDRROutputIndex(1); // compute second input
	drrOrigin[0] -= 10;
	drrOrigin[1] += 15;
	drrOrigin[2] -= 20;
	ProjectionGeometry->SetDetectorOrigin(drrOrigin);
	DRRFilter->SetProjectionGeometry(1, ProjectionGeometry);
	try
	{
		DRRFilter->Update();
	}
	catch (...)
	{
		lok = false;
	}
	// first check whether 1st output changed (expected: UNCHANGED):
	DRRFilter->GetOutput(0)->Update(); // additionally provoke update!!!
	tregion = DRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	int refArr2Size = tregion.GetNumberOfPixels();
	auto *refArr2 = new PixelType[refArr2Size];
	IntensityIteratorType dit1a(DRRFilter->GetOutput(0), tregion);
	c = 0;
	for (dit1a.GoToBegin(); !dit1a.IsAtEnd(); ++dit1a)
		refArr2[c++] = dit1a.Get();
	if (refArr1Size == refArr2Size)
	{
		for (c = 0; c < refArr1Size; c++)
		{
			if (itk::Math::Round<int, double>(refArr1[c] * ACCURACY) !=
				itk::Math::Round<int, double>(refArr2[c] * ACCURACY))
			{
				lok = false;
				break;
			}
		}
	}
	else
	{
		lok = false;
	}
	// now check whether 2nd output is different from 1st (expected: CHANGED):
	delete[] refArr2;
	tregion = DRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	refArr2Size = tregion.GetNumberOfPixels();
	refArr2 = new PixelType[refArr2Size];
	IntensityIteratorType dit2(DRRFilter->GetOutput(1), tregion);
	c = 0;
	for (dit2.GoToBegin(); !dit2.IsAtEnd(); ++dit2)
		refArr2[c++] = dit2.Get();
	if (refArr1Size == refArr2Size)
	{
		auto numDiffPixels = 0;
		for (c = 0; c < refArr1Size; c++)
		{
			if (itk::Math::Round<int, double>(refArr1[c] * ACCURACY) !=
				itk::Math::Round<int, double>(refArr2[c] * ACCURACY))
				numDiffPixels++;
		}
		if (numDiffPixels == 0)
			lok = false;
	}
	else // changed, but not in size expected!
	{
		lok = false;
	}

	// change some DRR-settings:
	DRRFilter->SetCurrentDRROutputIndex(2); // compute third input
	auto itf2 = ora::IntensityTransferFunction<PixelType, PixelType>::New();
	itf2->AddSupportingPoint(0, 0.05);
	itf2->AddSupportingPoint(500, 0.07);
	itf2->AddSupportingPoint(1001, 0.12);
	itf2->AddSupportingPoint(1200, 0.16);
	itf2->AddSupportingPoint(1201, 0.16);
	itf2->AddSupportingPoint(2500, 0.3);
	itf2->AddSupportingPoint(3000, 0.3);

	DRRFilter->SetITF(itf2);
	try
	{
		DRRFilter->Update();
	}
	catch (...)
	{
		lok = false;
	}
	// first check whether 1st output changed (expected: UNCHANGED):
	DRRFilter->GetOutput(0)->Update(); // additionally provoke update!!!
	tregion = DRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	int refArr3Size = tregion.GetNumberOfPixels();
	auto *refArr3 = new PixelType[refArr3Size];
	IntensityIteratorType dit1b(DRRFilter->GetOutput(0), tregion);
	c = 0;
	for (dit1b.GoToBegin(); !dit1b.IsAtEnd(); ++dit1b)
		refArr3[c++] = dit1b.Get();
	if (refArr1Size == refArr3Size)
	{
		for (c = 0; c < refArr1Size; c++)
		{
			if (itk::Math::Round<int, double>(refArr1[c] * ACCURACY) !=
				itk::Math::Round<int, double>(refArr3[c] * ACCURACY))
			{
				lok = false;
				break;
			}
		}
	}
	else
	{
		lok = false;
	}
	// check whether 2nd output changed (expected: UNCHANGED):
	DRRFilter->GetOutput(1)->Update(); // additionally provoke update!!!
	tregion = DRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	refArr3Size = tregion.GetNumberOfPixels();
	delete[] refArr3;
	refArr3 = new PixelType[refArr3Size];
	IntensityIteratorType dit1c(DRRFilter->GetOutput(1), tregion);
	c = 0;
	for (dit1c.GoToBegin(); !dit1c.IsAtEnd(); ++dit1c)
		refArr3[c++] = dit1c.Get();
	if (refArr2Size == refArr3Size)
	{
		for (c = 0; c < refArr2Size; c++)
		{
			if (itk::Math::Round<int, double>(refArr2[c] * ACCURACY) !=
				itk::Math::Round<int, double>(refArr3[c] * ACCURACY))
			{
				lok = false;
				break;
			}
		}
	}
	else
	{
		lok = false;
	}
	// now check whether 3rd output is different from 1st/2nd (expected: CHANGED):
	delete[] refArr3;
	tregion = DRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	refArr3Size = tregion.GetNumberOfPixels();
	refArr3 = new PixelType[refArr3Size];
	IntensityIteratorType dit3(DRRFilter->GetOutput(2), tregion);
	c = 0;
	for (dit3.GoToBegin(); !dit3.IsAtEnd(); ++dit3)
		refArr3[c++] = dit3.Get();
	if (refArr1Size == refArr3Size && refArr2Size == refArr3Size)
	{
		auto numDiffPixels = 0;
		for (c = 0; c < refArr3Size; c++)
		{
			if (itk::Math::Round<int, double>(refArr1[c] * ACCURACY) !=
				itk::Math::Round<int, double>(refArr2[c] * ACCURACY) &&
				itk::Math::Round<int, double>(refArr1[c] * ACCURACY) !=
				itk::Math::Round<int, double>(refArr3[c] * ACCURACY))
				numDiffPixels++;
		}
		if (numDiffPixels == 0)
			lok = false;
	}
	else // changed, but not in size expected!
	{
		lok = false;
	}
	delete[] refArr1;
	delete[] refArr2;
	delete[] refArr3;

	if (ImageOutput)
	{
		auto dw = DRRWriterType::New();
		dw->SetFileName("indep_Siddon_0.mhd");
		dw->SetInput(DRRFilter->GetOutput(0));
		try
		{
			dw->Update();
		}
		catch (...)
		{
			lok = false;
		}
		dw->SetFileName("indep_Siddon_1.mhd");
		dw->SetInput(DRRFilter->GetOutput(1));
		try
		{
			dw->Update();
		}
		catch (...)
		{
			lok = false;
		}
		dw->SetFileName("indep_Siddon_2.mhd");
		dw->SetInput(DRRFilter->GetOutput(2));
		try
		{
			dw->Update();
		}
		catch (...)
		{
			lok = false;
		}
		dw = nullptr;
	}
	DRRFilter->SetNumberOfIndependentOutputs(1);

	// reset geometry for cuda test
	drrOrigin[0] += 10;
	drrOrigin[1] -= 15;
	drrOrigin[2] += 20;
	ProjectionGeometry->SetDetectorOrigin(drrOrigin);

	// Cuda DRR Testing
	CudaDRRFilter->SetNumberOfIndependentOutputs(3);
	CudaDRRFilter->SetCurrentDRROutputIndex(0);
	if (!CudaDRRFilter->GetOutput(0) || !CudaDRRFilter->GetOutput(1) || !CudaDRRFilter->GetOutput(2))
		culok = false;

	// largest possible region of the un-computed outputs must be 0!
	CudaDRRImageType::RegionType cutregion;
	cutregion = CudaDRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() <= 0)
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() != 0)
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() != 0)
		culok = false;
	CudaDRRFilter->SetCurrentDRROutputIndex(1); // compute 2nd output
	CudaDRRFilter->SetGeometry(1, ProjectionGeometry);
	try
	{
		CudaDRRFilter->Update();
	}
	catch (...)
	{
		culok = false;
	}

	if (!CudaDRRFilter->GetOutput(0) || !CudaDRRFilter->GetOutput(1) || !CudaDRRFilter->GetOutput(2))
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() <= 0)
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() <= 0)
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() != 0)
		culok = false;
	CudaDRRFilter->SetCurrentDRROutputIndex(2); // compute 3rd output
	CudaDRRFilter->SetGeometry(2, ProjectionGeometry);
	try
	{
		CudaDRRFilter->Update();
	}
	catch (...)
	{
		culok = false;
	}

	if (!CudaDRRFilter->GetOutput(0) || !CudaDRRFilter->GetOutput(1) || !CudaDRRFilter->GetOutput(2))
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() <= 0)
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() <= 0)
		culok = false;
	cutregion = CudaDRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	if (cutregion.GetNumberOfPixels() <= 0)
		culok = false;
	
	// now check whether the outputs are REALLY independent from each other:
	CudaDRRFilter->SetNumberOfIndependentOutputs(3); // 3 independent outputs again
	CudaDRRFilter->SetCurrentDRROutputIndex(0); // compute first input (again)
	CudaDRRFilter->SetProjectionGeometry(1, ProjectionGeometry); //resetting projection geometries
	CudaDRRFilter->SetProjectionGeometry(2, ProjectionGeometry);
	try
	{
		CudaDRRFilter->Update();
	}
	catch (...)
	{
		culok = false;
	}
	// store image intensities in a reference array:
	cutregion = CudaDRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	int curefArr1Size = cutregion.GetNumberOfPixels();
	auto *curefArr1 = new PixelType[curefArr1Size];
	CudaIntensityIteratorType cudit1(CudaDRRFilter->GetOutput(0), cutregion);
	auto cuc = 0;
	for (cudit1.GoToBegin(); !cudit1.IsAtEnd(); ++cudit1)
		curefArr1[cuc++] = cudit1.Get();
	// change some DRR-props:
	CudaDRRFilter->SetCurrentDRROutputIndex(1); // compute second input
	drrOrigin[0] -= 10;
	drrOrigin[1] += 15;
	drrOrigin[2] -= 20;
	ProjectionGeometry->SetDetectorOrigin(drrOrigin);
	CudaDRRFilter->SetProjectionGeometry(1, ProjectionGeometry);
	try
	{
		CudaDRRFilter->Update();
	}
	catch (...)
	{
		culok = false;
	}
	// first check whether 1st output changed (expected: UNCHANGED):
	CudaDRRFilter->GetOutput(0)->Update(); // additionally provoke update!!!
	cutregion = CudaDRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	int curefArr2Size = cutregion.GetNumberOfPixels();
	auto *curefArr2 = new PixelType[curefArr2Size];
	CudaIntensityIteratorType cudit1a(CudaDRRFilter->GetOutput(0), cutregion);
	cuc = 0;
	for (cudit1a.GoToBegin(); !cudit1a.IsAtEnd(); ++cudit1a)
		curefArr2[cuc++] = cudit1a.Get();
	if (curefArr1Size == curefArr2Size)
	{
		for (cuc = 0; cuc < curefArr1Size; cuc++)
		{
			if (itk::Math::Round<int, double>(curefArr1[cuc] * ACCURACY) !=
				itk::Math::Round<int, double>(curefArr2[cuc] * ACCURACY))
			{
				culok = false;
				break;
			}
		}
	}
	else
	{
		culok = false;
	}
	// now check whether 2nd output is different from 1st (expected: CHANGED):
	delete[] curefArr2;
	cutregion = CudaDRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	curefArr2Size = cutregion.GetNumberOfPixels();
	curefArr2 = new PixelType[curefArr2Size];
	CudaIntensityIteratorType cudit2(CudaDRRFilter->GetOutput(1), cutregion);
	cuc = 0;
	for (cudit2.GoToBegin(); !cudit2.IsAtEnd(); ++cudit2)
		curefArr2[cuc++] = cudit2.Get();
	if (curefArr1Size == curefArr2Size)
	{
		auto numDiffPixels = 0;
		for (cuc = 0; cuc < curefArr1Size; cuc++)
		{
			if (itk::Math::Round<int, double>(curefArr1[cuc] * ACCURACY) !=
				itk::Math::Round<int, double>(curefArr2[cuc] * ACCURACY))
				numDiffPixels++;
		}
		if (numDiffPixels == 0)
			culok = false;
	}
	else // changed, but not in size expected!
	{
		culok = false;
	}

	// change some DRR-settings:
	CudaDRRFilter->SetCurrentDRROutputIndex(2); // compute third input
	CudaDRRFilter->SetITF(itf2);
	CudaDRRFilter->SetOffTheFlyITFMapping(true);
	
	try
	{
		CudaDRRFilter->Update();
	}
	catch (...)
	{
		culok = false;
	}
	// first check whether 1st output changed (expected: UNCHANGED):
	CudaDRRFilter->GetOutput(0)->Update(); // additionally provoke update!!!
	cutregion = CudaDRRFilter->GetOutput(0)->GetLargestPossibleRegion();
	int curefArr3Size = cutregion.GetNumberOfPixels();
	auto *curefArr3 = new PixelType[curefArr3Size];
	IntensityIteratorType cudit1b(CudaDRRFilter->GetOutput(0), cutregion);
	cuc = 0;
	for (cudit1b.GoToBegin(); !cudit1b.IsAtEnd(); ++cudit1b)
		curefArr3[cuc++] = cudit1b.Get();
	if (curefArr1Size == curefArr3Size)
	{
		for (cuc = 0; cuc < curefArr1Size; cuc++)
		{
			if (itk::Math::Round<int, double>(curefArr1[cuc] * ACCURACY) !=
				itk::Math::Round<int, double>(curefArr3[cuc] * ACCURACY))
			{
				culok = false;
				break;
			}
		}
	}
	else
	{
		culok = false;
	}
	// check whether 2nd output changed (expected: UNCHANGED):
	CudaDRRFilter->GetOutput(1)->Update(); // additionally provoke update!!!
	cutregion = CudaDRRFilter->GetOutput(1)->GetLargestPossibleRegion();
	curefArr3Size = cutregion.GetNumberOfPixels();
	delete[] curefArr3;
	curefArr3 = new PixelType[curefArr3Size];
	IntensityIteratorType cudit1c(CudaDRRFilter->GetOutput(1), cutregion);
	cuc = 0;
	for (cudit1c.GoToBegin(); !cudit1c.IsAtEnd(); ++cudit1c)
		curefArr3[cuc++] = cudit1c.Get();
	if (curefArr2Size == curefArr3Size)
	{
		for (cuc = 0; cuc < curefArr2Size; cuc++)
		{
			if (itk::Math::Round<int, double>(curefArr2[cuc] * ACCURACY) !=
				itk::Math::Round<int, double>(curefArr3[cuc] * ACCURACY))
			{
				culok = false;
				break;
			}
		}
	}
	else
	{
		culok = false;
	}
	// now check whether 3rd output is different from 1st/2nd (expected: CHANGED):
	delete[] curefArr3;
	cutregion = CudaDRRFilter->GetOutput(2)->GetLargestPossibleRegion();
	curefArr3Size = cutregion.GetNumberOfPixels();
	curefArr3 = new PixelType[curefArr3Size];
	IntensityIteratorType cudit3(CudaDRRFilter->GetOutput(2), cutregion);
	cuc = 0;
	for (cudit3.GoToBegin(); !cudit3.IsAtEnd(); ++cudit3)
		curefArr3[cuc++] = cudit3.Get();
	if (curefArr1Size == curefArr3Size && curefArr2Size == curefArr3Size)
	{
		auto numDiffPixels = 0;
		for (cuc = 0; cuc < curefArr3Size; cuc++)
		{
			if (itk::Math::Round<int, double>(curefArr1[cuc] * ACCURACY) !=
				itk::Math::Round<int, double>(curefArr2[cuc] * ACCURACY) &&
				itk::Math::Round<int, double>(curefArr1[cuc] * ACCURACY) !=
				itk::Math::Round<int, double>(curefArr3[cuc] * ACCURACY))
				numDiffPixels++;
		}
		if (numDiffPixels == 0)
			culok = false;
	}
	else // changed, but not in size expected!
	{
		culok = false;
	}

	if (ImageOutput)
	{
		auto cudw = CudaDRRWriterType::New();
		cudw->SetFileName("indep_Cuda_0.mhd");
		cudw->SetInput(CudaDRRFilter->GetOutput(0));
		try
		{
			cudw->Update();
		}
		catch (...)
		{
			culok = false;
		}
		cudw->SetFileName("indep_Cuda_1.mhd");
		cudw->SetInput(CudaDRRFilter->GetOutput(1));
		try
		{
			cudw->Update();
		}
		catch (...)
		{
			culok = false;
		}
		cudw->SetFileName("indep_Cuda_2.mhd");
		cudw->SetInput(CudaDRRFilter->GetOutput(2));
		try
		{
			cudw->Update();
		}
		catch (...)
		{
			culok = false;
		}
		cudw = nullptr;
	}
	CudaDRRFilter->SetNumberOfIndependentOutputs(1);

	delete[] curefArr1;
	delete[] curefArr2;
	delete[] curefArr3;

	ok = ok && lok && culok;
	VERBOSE(Verbose, << "CudaDRRFilter: " << (culok ? "OK" : "FAILURE") << "\n");
	VERBOSE(Verbose, << "DRRFilter: " << (lok ? "OK" : "FAILURE") << "\n");

	VERBOSE(Verbose, << "\n  * Testing changed size for volume and detector... \n");
	lok = true;
	culok = true;

	// changed image volume
	auto ImageScalingFactor = 2;
	for( auto i = 0; i < 3; ++i)
	{
		isize[i] *= ImageScalingFactor;
		ispacing[i] /= ImageScalingFactor;

		cusize[i] *= ImageScalingFactor;
		cuspacing[i] /= ImageScalingFactor;
	}
	iregion.SetSize(isize);
	curegion.SetSize(cusize);
	auto cuvolumeT = CudaVolumeImageType::New();
	cuvolumeT->SetRegions(curegion);
	cuvolumeT->Allocate();
	cuvolumeT->SetSpacing(cuspacing);
	cuvolumeT->SetOrigin(cuorigin);

	auto volumeT = VolumeImageType::New();
	volumeT->SetRegions(iregion);
	volumeT->Allocate();
	volumeT->SetSpacing(ispacing);
	volumeT->SetOrigin(iorigin);

	// fill volume with random values
	VolumeIteratorType itT(volumeT, iregion);
	VolumeImageType::IndexType idxT;
	CudaVolumeIteratorType cuitT(cuvolumeT, curegion);

	srand(time(nullptr));
	for (itT.GoToBegin(), cuitT.GoToBegin(); !itT.IsAtEnd(); ++itT, ++cuitT)
	{
		idxT = itT.GetIndex();
		if (idxT[0] > 10 * ImageScalingFactor && idxT[0] < 70 * ImageScalingFactor 
			&& idxT[1] > 5 * ImageScalingFactor && idxT[1] < 65 * ImageScalingFactor
			&& idxT[2] > 2 * ImageScalingFactor && idxT[2] < 28 * ImageScalingFactor)
		{
			v = rand() % 1000 + 1000; // tissue
		}
		else
		{
			v = 0; // air
		}
		itT.Set(v);
		cuitT.Set(v);
	}

	if(ImageOutput)
	{
		auto w = VolumeWriterType::New();
		w->SetInput(volumeT);
		w->SetFileName("volumeT.mhd");
		try
		{
			w->Update();
		}
		catch (itk::ExceptionObject &EO)
		{
			lok = false;
			EO.Print(std::cout);
		}
		w = nullptr;

		auto cuw = CudaVolumeWriterType::New();
		cuw->SetInput(cuvolumeT);
		cuw->SetFileName("cuvolumeT.mhd");
		try
		{
			cuw->Update();
		}
		catch (itk::ExceptionObject &EO)
		{
			culok = false;
			EO.Print(std::cout);
		}
		cuw = nullptr;
	}

	// changed detector geometry by scaling factor
	auto DetectorScalingFactor = 10;
	for (auto i = 0; i < 2; ++i) // change only the first two parameters
	{
		drrSize[i] *= DetectorScalingFactor;
		drrSpacing[i] /= DetectorScalingFactor;
	}
	std::cout << "New detector size: " << drrSize[0] << " " << drrSize[1] << std::endl;
	std::cout << "New detector spacing: " << drrSpacing[0] << " " << drrSpacing[1] << std::endl;
	ProjectionGeometry->SetDetectorSize(drrSize);
	ProjectionGeometry->SetDetectorPixelSpacing(drrSpacing);

	// Siddon large detector timing
	DRRFilter->SetCurrentDRROutputIndex(0);
	DRRFilter->SetProjectionGeometry(0, ProjectionGeometry);

	auto start = Clock::now();
	try
	{
		DRRFilter->Update();
	}
	catch(...)
	{
		lok = false;
	}

	auto duration = std::chrono::duration_cast<msType>(Clock::now() - start);
	duration.count();
	std::cout << "Large Detector (Siddon): " << duration.count() << " ms." << std::endl;
	if (ImageOutput)
	{
		auto tw = DRRWriterType::New();
		tw->SetFileName("largeDet_Siddon.mhd");
		tw->SetInput(DRRFilter->GetOutput(0));
		try
		{
			tw->Update();
		}
		catch (...)
		{
			lok = false;
		}
		tw = nullptr;
	}

	// Siddon large detector and volume timing
	DRRFilter->SetInput(volumeT);
	start = Clock::now();
	try
	{
		DRRFilter->Update();
	}
	catch(...)
	{
		lok = false;
	}
	duration = std::chrono::duration_cast<msType>(Clock::now() - start);
	duration.count();
	std::cout << "Large Detector and volume (Siddon): " << duration.count() << " ms." << std::endl;
	if (ImageOutput)
	{
		auto tw = DRRWriterType::New();
		tw->SetFileName("largeDetVol_Siddon.mhd");
		tw->SetInput(DRRFilter->GetOutput(0));
		try
		{
			tw->Update();
		}
		catch (...)
		{
			lok = false;
		}
		tw = nullptr;
	}

	// Cuda large detector timing
	CudaDRRFilter->SetCurrentDRROutputIndex(0);
	CudaDRRFilter->SetGeometry(0, ProjectionGeometry);

	start = Clock::now();
	try
	{
		CudaDRRFilter->Update();
	}
	catch (...)
	{
		culok = false;
	}

	duration = std::chrono::duration_cast<msType>(Clock::now() - start);
	duration.count();
	std::cout << "Large Detector (Cuda): " << duration.count() << " ms." << std::endl;

	if (ImageOutput)
	{
		auto cutw = CudaDRRWriterType::New();
		cutw->SetFileName("largeDet_Cuda.mhd");
		cutw->SetInput(CudaDRRFilter->GetOutput(0));
		try
		{
			cutw->Update();
		}
		catch (...)
		{
			culok = false;
		}
		cutw = nullptr;
	}

	// Siddon large detector and volume timing
	CudaDRRFilter->SetInput(cuvolumeT);
	start = Clock::now();
	try
	{
		CudaDRRFilter->Update();
	}
	catch (...)
	{
		culok = false;
	}

	duration = std::chrono::duration_cast<msType>(Clock::now() - start);
	duration.count();
	std::cout << "Large Detector and volume (Cuda): " << duration.count() << " ms." << std::endl;

	if (ImageOutput)
	{
		auto cutw = CudaDRRWriterType::New();
		cutw->SetFileName("largeDetVol_Cuda.mhd");
		cutw->SetInput(CudaDRRFilter->GetOutput(0));
		try
		{
			cutw->Update();
		}
		catch (...)
		{
			culok = false;
		}
		cutw = nullptr;
	}

	ok = ok && lok && culok;
	VERBOSE(Verbose, << "CudaDRRFilter: " << (culok ? "OK" : "FAILURE") << "\n");
	VERBOSE(Verbose, << "DRRFilter: " << (lok ? "OK" : "FAILURE") << "\n");

	VERBOSE(Verbose, << "\n  * Final reference count check ...\n");
	lok = true;
	culok = true;

	if (DRRFilter->GetReferenceCount() != 1)
		lok = false;
	if (CudaDRRFilter->GetReferenceCount() != 1)
		culok = false;
	ok = ok && lok && culok;
	VERBOSE(Verbose, << "CudaDRRFilter: " << (culok ? "OK" : "FAILURE") << "\n");
	VERBOSE(Verbose, << "DRRFilter: " << (lok ? "OK" : "FAILURE") << "\n");

	DRRFilter = nullptr; // reference counter must be zero!
	CudaDRRFilter = nullptr;

	VERBOSE(Verbose, << "\nTest result:.......");
	if (ok)
	{
		VERBOSE(Verbose, << "OK\n")
		return EXIT_SUCCESS;
	}
	VERBOSE(Verbose, << "FAILURE\n");
	return EXIT_FAILURE;
}