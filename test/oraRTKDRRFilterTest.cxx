// TESTED CLASS
#include "oraRTKDRRFilter.h"

//STL
#include <stdlib.h>
#include <string>

// ITK
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkTestingMacros.h"

// ORA
#include "oraProjectionGeometry.h"
#include "CommonToolsNew.hxx"
#include <itkEuler3DTransform.h>


void PrintUsage(std::string programmname)
{
	std::cerr << "USAGE: \n";
	std::cerr << programmname << "[options]" << " <inputimage> <outputdirectory>" <<"\n\n";

	std::cerr << "options:\n";
	std::cerr << "\t -v or --verbose\t\t -print debug information\n";
	std::cerr << "\t -io or --image-output\t\t -write images to current folder\n";
	std::cerr << "\t -h or --help\t\t -print this information\n";

	std::cerr << std::endl;
}

template<class TInputImage>
void WriteImage(TInputImage Image, std::string Directory, std::string Filename)
{
	using WriterType = itk::ImageFileWriter<TInputImage>;
	auto Writer = WriterType::New();

	Writer->SetInput(Image);
	//Writer->SetFileName()

}

/*************************   TEST FUNCTION   *******************************/
int oraRTKDRRFilterTest(int argc, char * argv[])
{
	// input argument number check
	if (argc < 3)
	{
		PrintUsage(argv[0]);
		return EXIT_FAILURE;
	}
	// input argument parsing
	auto Verbose = false;
	auto ImageOutput = false;
	for (auto i = 1; i < argc; ++i)
		{
			if (std::string(argv[i]) == "-v" || std::string(argv[i]) == "--verbose")
			{
				Verbose = true;
				continue;
			}
			if (std::string(argv[i]) == "-io" || std::string(argv[i]) == "--image-output")
			{
				ImageOutput = true;
				continue;
			}
			if (std::string(argv[i]) == "-h" || std::string(argv[i]) == "--help")
			{
				PrintUsage(argv[0]);
				return EXIT_SUCCESS;
			}
		}

	const std::string InputImage = argv[argc - 2];
	const std::string OutputDirectory = argv[argc - 1];

	// Read input image
	constexpr auto Dimension = 3;
	using PixelType = float;
	using ImageType = itk::CudaImage<PixelType, Dimension>;
	using ReaderType = itk::ImageFileReader<ImageType>;

	auto Reader = ReaderType::New();
	Reader->SetFileName(InputImage);
	TRY_EXPECT_NO_EXCEPTION(Reader->Update());
	Reader->UpdateLargestPossibleRegion();

	VERBOSE(Verbose, << " * Generating and applying DRR properties ... ");
	double SourcePosition[]         = { 100, 10, 800 };
	double DetectorRowOrientation[] = { 1, 0, 0 };
	double DetectorColOrientation[] = { 0, 1, 0 };
	double DetectorOrigin[]         = { -100, -80, -150 };
	int    DetectorSize[]           = { 200, 160 };
	double DetectorSpacing[]        = { 1.0, 1.0 };

	using ProjectionGeometryType = ora::ProjectionGeometry;
	auto ProjectionGeometry = ProjectionGeometryType::New();
	ProjectionGeometry->SetSourcePosition(SourcePosition);
	ProjectionGeometry->SetDetectorRowOrientation(DetectorRowOrientation);
	ProjectionGeometry->SetDetectorColumnOrientation(DetectorColOrientation);
	ProjectionGeometry->SetDetectorPixelSpacing(DetectorOrigin);
	ProjectionGeometry->SetDetectorSize(DetectorSize);
	ProjectionGeometry->SetDetectorPixelSpacing(DetectorSpacing);

	using TransformType = itk::Euler3DTransform<double>;
	auto Transform = TransformType::New();
	Transform->SetIdentity();

	using DRRFilterType = ora::RTKDRRFilter<PixelType, PixelType>;
	auto DRRFilter = DRRFilterType::New();
	DRRFilter->SetNumberOfIndependentOutputs(1);
	DRRFilter->SetGeometry(0, ProjectionGeometry);
	DRRFilter->SetInput(Reader->GetOutput());
	DRRFilter->SetTransform(Transform);




	return EXIT_SUCCESS;
}