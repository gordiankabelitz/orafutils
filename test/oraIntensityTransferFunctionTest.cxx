//std
#include <fstream>
#include <sstream>
//me
#include "oraIntensityTransferFunction.h"
//ITK
#include <itkRealTimeClock.h>
#include "CommonToolsNew.hxx"

// write CSV flag
bool WriteCSV = false;

/** Helper for string replacement. **/
std::string Replace(std::string str, const std::string& from,
                    const std::string& to)
{
  std::size_t start_pos = str.find(from);
  while (start_pos != std::string::npos)
  {
    str.replace(start_pos, from.length(), to);
    start_pos = str.find(from);
  }

  return str;
}

/**
 * Tests base functionality of:
 *
 *   ora::IntensityTransferFunction.cxx
 *
 * Test application result is 0 (EXIT_SUCCESS) if SUCCESSFUL.
 *
 * Arguments: run test with -h or --help option to explore them!
 *
 * @see ora::IntensityTransferFunction
 *
 * @author jeanluc
 * @author phil
 * @version 1.1
 *
 * \ingroup Tests
 */
int oraIntensityTransferFunctionTest(int argc, char *argv[])
{
  // basic command line pre-processing:
	auto Verbose = false;
	for (auto i = 1; i < argc; i++)
	{
		if (std::string(argv[i]) == "-v" || std::string(argv[i]) == "--verbose")
		{
			Verbose = true;
			continue;
		}
		if (std::string(argv[i]) == "-h" || std::string(argv[i]) == "--help")
		{
			std::cout << "No help information needed.\n"
				<< "USAGE: oraIntensityTransferFunctionTest [options]" << std::endl;
			return EXIT_SUCCESS;
		}
	}
  // advanced arguments:
  for (auto i = 1; i < argc; i++)
  {
    if (std::string(argv[i]) == "-wcsv" || std::string(argv[i])
        == "--write-csv")
    {
      WriteCSV = true;
      continue;
    }
  }

  using ITFType = ora::IntensityTransferFunction<double, double>;
  using ITFPointer = ITFType::Pointer;

  auto ok = true; // OK-flag for whole test
  bool lok; // local OK-flag for a test sub-section

  VERBOSE(Verbose, << "\nTesting intensity transfer function interface.\n")

  VERBOSE(Verbose, << "  * Basic configuration ... ")
  lok = true; // initialize sub-section's success state

  {
    auto itf = ITFType::New();
    //adding support points
    itf->AddSupportingPoint(0.0, 0.0);
    itf->AddSupportingPoint(100.0, 100.0);
    //checking if output value correct for input 50
    if (itf->MapInValue(50.0) != 50.0)
      lok = false;
    //adding another supporting point
    itf->AddSupportingPoint(50.0, 25.0);
    //checking of input is mapped to right values
    if (itf->MapInValue(12.5) != 6.25)
      lok = false;
    if (itf->MapInValue(75.0) != 62.5)
    lok = false;
    //checking if it returns the right number of supporting points
    if (itf->GetNumberOfSupportingPoints() != 3)
      lok = false;
    // try to retrieve all supporting numbers and check
    // if output is sorted (as it should be after calling a map function
    const std::vector<ora::SupportingPointStruct> *supportingPointValues;
    supportingPointValues = itf->GetSupportingPoints();
    if((*supportingPointValues)[0].inValue != 0.0 ||
        (*supportingPointValues)[0].outValue != 0.0 ||
        (*supportingPointValues)[1].inValue != 50.0 ||
        (*supportingPointValues)[1].outValue != 25.0||
        (*supportingPointValues)[2].inValue != 100.0||
        (*supportingPointValues)[2].outValue != 100.0)
      lok = false;
    // removing support point and check if updated correctly
    itf->RemoveSupportingPoint(1);
    supportingPointValues = itf->GetSupportingPoints();
    if((*supportingPointValues)[0].inValue != 0.0 ||
        (*supportingPointValues)[1].inValue != 100.0 ||
        (*supportingPointValues)[0].outValue != 0.0 ||
        (*supportingPointValues)[1].outValue != 100.0)
      lok = false;
    //add new supporting point and check mapping
    itf->AddSupportingPoint(70.0, 50.0);
    double temp = 0;
    itf->MapInValue(85.0, temp);
    if(temp != 75.0)
      lok = false;
    //removing all points and check if it worked
    itf->RemoveAllSupportingPoints();
    if(itf->GetNumberOfSupportingPoints() != 0)
    lok = false;
    // adding range of supporting point and map in array of values
    itf->AddSupportingPoint(0.0, 0.0);
    itf->AddSupportingPoint(100.0, 100.0);
    itf->AddSupportingPoint(250.0, 200.0);
    itf->AddSupportingPoint(200.0, 150.0);
    double ins[10] = {-10.0, 22.0, 50.0, 200.0, 100.10, -30.0, 132.2, 160.9, 226.11, 400.0};
    double out[10];
    itf->MapInValues(10, ins, out);
    //check clamping
    for(int i = 0; i < 10; i++)
    {
      if((i == 0 || i == 5 || i == 9))
      {
        if(out[i] != 0)
          lok = false;
      }
      else if(out[i] == 0)
      {
        lok = false;
      }
    }
    //check output
    std::ostringstream os;
    itf->Print(os, 0);
    if (os.str().length() <= 0)
      lok = false;
  }
  ok = ok && lok; // update OK-flag for test-scope
  VERBOSE(Verbose, << (lok ? "OK" : "FAILURE") << "\n")


  VERBOSE(Verbose, << "  * Interpolating function ... ")
  lok = true; // initialize sub-section's success state
  {
    auto itf = ITFType::New();
    // adding support points
    itf->AddSupportingPoint(10.0, 10.0);
    itf->AddSupportingPoint(30.0, 30.0);
    itf->AddSupportingPoint(70.0, 100.0);
    itf->AddSupportingPoint(90.0, -30.0);
    itf->AddSupportingPoint(120.0, 40.0);
    itf->AddSupportingPoint(220.0, -50.0);
    itf->AddSupportingPoint(1400.0, 210.0);
    // interpolate function (clamp to zero)
    std::vector<double> ovs;
    std::ostringstream os;
    itf->SetExtrapolation(false);
    os << "interpolated-input;interpolated-output;supporting-input;supporting-output\n";
    unsigned int i = 0;
    for (auto iv = -100.; iv < 1800.; iv += 1., i++)
    {
      ovs.push_back(itf->MapInValue(iv));
      os << iv << ";" << ovs[ovs.size() - 1];
      if (i < itf->GetNumberOfSupportingPoints())
      {
        os << ";" << (*itf->GetSupportingPoints())[i].inValue << ";"
              << (*itf->GetSupportingPoints())[i].outValue;
      }
      os << "\n";
    }
    if (WriteCSV)
    {
      std::ofstream ofi;
      ofi.open("interpolated1.csv");
      ofi << Replace(os.str(), ".", ",");
      ofi.close();
    }
    // interpolate function (extrapolation)
    ovs.clear();
    os.str("");
    itf->SetExtrapolation(true);
    os << "interpolated-input;interpolated-output;supporting-input;supporting-output\n";
    i = 0;
    for (auto iv = -100.; iv < 1800.; iv += 1., i++)
    {
      ovs.push_back(itf->MapInValue(iv));
      os << iv << ";" << ovs[ovs.size() - 1];
      if (i < itf->GetNumberOfSupportingPoints())
      {
        os << ";" << (*itf->GetSupportingPoints())[i].inValue << ";"
              << (*itf->GetSupportingPoints())[i].outValue;
      }
      os << "\n";
    }
    if (WriteCSV)
    {
      std::ofstream ofi;
      ofi.open("interpolated2.csv");
      ofi << Replace(os.str(), ".", ",");
      ofi.close();
    }
    // time measurement
    itk::RealTimeClock::Pointer clock = itk::RealTimeClock::New();

    double ts = clock->GetTimeInSeconds();
    i = 0;
    for (double iv = -100.; iv < 1800.; iv += .1, i++)
    {
      ovs.push_back(itf->MapInValue(iv));
    }
    ts = (clock->GetTimeInSeconds() - ts) * 1e6 / static_cast<double>(i);

    VERBOSE(Verbose, << " [" << ts << " us per call on average] ... ")
  }
  ok = ok && lok; // update OK-flag for test-scope
  VERBOSE(Verbose, << (lok ? "OK" : "FAILURE") << "\n")


  VERBOSE(Verbose, << "Test result: ")
  if (ok)
  {
    VERBOSE(Verbose, << "OK\n\n")
    return EXIT_SUCCESS;
  }
  else
  {
    VERBOSE(Verbose, << "FAILURE\n\n")
    return EXIT_FAILURE;
  }
}
