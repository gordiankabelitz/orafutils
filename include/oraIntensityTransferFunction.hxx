#ifndef ORAIntensityTransferFunction_hxx
#define ORAIntensityTransferFunction_hxx

#include "oraIntensityTransferFunction.h"
#include <vector>
#include <algorithm>

namespace ora
{
	template<typename TInputPixel, typename TOutputPixel>
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::IntensityTransferFunction() : itk::Object()
	{
		m_LastSortTimeStamp = 0;
		m_Extrapolation = false; // clamp mode
	}

	template<typename TInputPixel, typename TOutputPixel>
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::~IntensityTransferFunction()
	{
	}

	template<typename TInputPixel, typename TOutputPixel>
	void
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::PrintSelf(std::ostream& os, itk::Indent indent) const
	{
		auto itIn = m_SupportingPoints.cbegin();
		do
		{
			os << indent << "input: " << (*itIn).inValue;
			os << " mapped to output: " << (*itIn).outValue;
			os << std::endl;
			++itIn;
		} while (itIn != m_SupportingPoints.end());
	}

	template<typename TInputPixel, typename TOutputPixel>
	void
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::AddSupportingPoint(double in, double out)
	{
		SupportingPointStruct point;
		point.inValue = in;
		point.outValue = out;
		m_SupportingPoints.push_back(point);
		this->Modified();
	}

	template<typename TInputPixel, typename TOutputPixel>
	unsigned int
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::GetNumberOfSupportingPoints() const
	{
		return m_SupportingPoints.size();
	}

	template<typename TInputPixel, typename TOutputPixel>
	void
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::RemoveSupportingPoint(int i)
	{
		if (i > static_cast<int>(GetNumberOfSupportingPoints()) || i < 0)
			return;
		const auto pos = m_SupportingPoints.begin() + i;
		m_SupportingPoints.erase(pos);
		this->Modified();
	}

	template<typename TInputPixel, typename TOutputPixel>
	void
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::RemoveAllSupportingPoints()
	{
		m_SupportingPoints.clear();
		this->Modified();
	}

	template<typename TInputPixel, typename TOutputPixel>
	const typename IntensityTransferFunction<TInputPixel,TOutputPixel>::Vector *
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::GetSupportingPoints() const
	{
		return &m_SupportingPoints;
	}

	template<typename TInputPixel, typename TOutputPixel>
	bool
	IntensityTransferFunction<TInputPixel,TOutputPixel>
	::DesignByString(const std::string &s)
	{
		// basic string check
		if (s.length() <= 0)
			return false;

		// check format: (space-separated)
		// <number-of-support-points> <in-point-1> <out-point-1> <in-point-2> <out-point-2> ...
		std::vector<std::string> toks;
		ora::Tokenize(s, toks, " ");
		if (toks.size() <= 1)
			return false;
		const int N = ora::StreamConvert<int, std::string>(toks[0]);
		if (N < 2)
			return false;
		if (toks.size() != (2 * N + 1))
			return false;

		// if correctly formatted, clear current ITF
		this->RemoveAllSupportingPoints();

		// fill the ITF with new supporting points
		auto x = 1;
		double i, o;
		while (x < (2 * N))
		{
			i = ora::StreamConvert<double, std::string>(toks[x++]);
			o = ora::StreamConvert<double, std::string>(toks[x++]);
			this->AddSupportingPoint(i, o);
		}

		// check if all points have been created
		if (this->GetNumberOfSupportingPoints() != N)
			return false;

		return true;
	}

	template <typename TInputPixel, typename TOutputPixel>
	TOutputPixel
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::MapInValue(TInputPixel in)
	{
		return static_cast<TOutputPixel>(GetOutputIntensity(static_cast<double>(in)));
	}

	template<typename TInputPixel, typename TOutputPixel>
	bool
	IntensityTransferFunction<TInputPixel,TOutputPixel>
	::SupportingPointsSmallerThan(const SupportingPointStruct &sp1, const SupportingPointStruct &sp2)
	{
		return sp1.inValue < sp2.inValue;
	}

	template<typename TInputPixel, typename TOutputPixel>
	double
	IntensityTransferFunction<TInputPixel,TOutputPixel>
	::GetOutputIntensity(double in)
	{
		if (m_SupportingPoints.empty())
			return 0;
		//check if the array needs sorting
		if (this->GetMTime() > m_LastSortTimeStamp)
		{
			m_LastSortTimeStamp = this->GetMTime();
			std::stable_sort(m_SupportingPoints.begin(), m_SupportingPoints.end(),
				SupportingPointsSmallerThan);
		}
		//don't check input that is not in range
		const std::size_t N = GetNumberOfSupportingPoints();
		if (in < m_SupportingPoints[0].inValue ||
			in > m_SupportingPoints[N - 1].inValue)
		{
			if (!m_Extrapolation || (m_Extrapolation && N < 2))
				return 0;
			// linear extrapolation
			if (in < m_SupportingPoints[0].inValue)
				return m_SupportingPoints[0].outValue
				+ (in - m_SupportingPoints[0].inValue)
				* (m_SupportingPoints[1].outValue - m_SupportingPoints[0].outValue)
				/ (m_SupportingPoints[1].inValue - m_SupportingPoints[0].inValue);
			else // in > m_SupportingPoints[N - 1].inValue)
				return m_SupportingPoints[N - 1].outValue
				+ (in - m_SupportingPoints[N - 1].inValue)
				* (m_SupportingPoints[N - 1].outValue - m_SupportingPoints[N - 2].outValue)
				/ (m_SupportingPoints[N - 1].inValue - m_SupportingPoints[N - 2].inValue);
		}
		//search for the right thresholds and return transfered intensity
		for (unsigned int i = 1; i < m_SupportingPoints.size(); i++)
		{
			if (m_SupportingPoints[i].inValue >= in)
			{
				return m_SupportingPoints[i - 1].outValue +
					((m_SupportingPoints[i].outValue - m_SupportingPoints[i - 1].outValue) *
					((in - m_SupportingPoints[i - 1].inValue) /
						(m_SupportingPoints[i].inValue - m_SupportingPoints[i - 1].inValue)));
			}
		}
		return 0;
	}

	template<typename TInputPixel, typename TOutputPixel>
	void
	IntensityTransferFunction<TInputPixel, TOutputPixel>
	::MapInValue(TInputPixel in, TOutputPixel& out)
	{
		out = static_cast<TOutputPixel>(GetOutputIntensity(static_cast<double>(in)));
	}

	template<typename TInputPixel, typename TOutputPixel>
	void
	IntensityTransferFunction<TInputPixel,TOutputPixel>
	::MapInValues(int count, TInputPixel* ins, TOutputPixel* outs)
	{
		for (auto i = 0; i < count; i++)
			outs[i] = static_cast<TOutputPixel>(GetOutputIntensity(static_cast<double>(ins[i])));
	}
}

#endif /* ORAIntensityTransferFunction_hxx */
