#ifndef ORAMetricEvents_hxx
#define ORAMetricEvents_hxx

#include <itkEventObject.h>

namespace ora
{

/**
 * Custom events for composite / multi metric classes.
 * @see ora::CompositeImageToImageMetric
 * @see ora::MultiImageToImageMetric
 **/
itkEventMacro(BeforeEvaluationEvent, itk::AnyEvent)
/**
 * Custom events for composite / multi metric classes.
 * @see ora::CompositeImageToImageMetric
 * @see ora::MultiImageToImageMetric
 **/
itkEventMacro(AfterEvaluationEvent, itk::AnyEvent)

}

#endif /* ORAMetricEvents_hxx */
