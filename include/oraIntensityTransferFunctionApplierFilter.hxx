#ifndef ORAIntensityTransferFunctionApplierFilter_hxx
#define ORAIntensityTransferFunctionApplierFilter_hxx

#include "oraIntensityTransferFunctionApplierFilter.h"

//ITK
#include <itkImageRegionConstIterator.h>
#include <itkImageRegionIterator.h>

namespace ora
{

template<typename TInputImage, typename TOutputImage>
IntensityTransferFunctionApplierFilter<TInputImage, TOutputImage>
::IntensityTransferFunctionApplierFilter()
{
  m_ITF = nullptr;
}

template<typename TInputImage, typename TOutputImage>
IntensityTransferFunctionApplierFilter<TInputImage, TOutputImage>
	::~IntensityTransferFunctionApplierFilter()
{
  m_ITF = nullptr;
}

template<typename TInputImage, typename TOutputImage>
void IntensityTransferFunctionApplierFilter<TInputImage, TOutputImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << " ITF-pointer: " << (ITFType*)m_ITF << "\n";
}

template<typename TInputImage, typename TOutputImage>
void IntensityTransferFunctionApplierFilter<TInputImage, TOutputImage>
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
  if (!m_ITF)
	itkExceptionMacro(<< "No ITF set!")

  // Get the input and output
  typename OutputImageType::Pointer Output = this->GetOutput();
  typename InputImageType::ConstPointer Input = this->GetInput();

  // map ITF values
  typedef itk::ImageRegionConstIterator<InputImageType> ConstIteratorType;
  typedef itk::ImageRegionIterator<OutputImageType> IteratorType;

  ConstIteratorType itInput(Input, outputRegionForThread);
  IteratorType itMapped(Output, outputRegionForThread);
  itInput.GoToBegin();
  itMapped.GoToBegin();
  do
  {
	itMapped.Set(static_cast<OutputPixelType>(m_ITF->MapInValue(static_cast<double>(itInput.Get()))));
	++itMapped;
	++itInput;
  } while (!itInput.IsAtEnd());
}

}

#endif /* ORAIntensityTransferFunctionApplierFilter_hxx */
