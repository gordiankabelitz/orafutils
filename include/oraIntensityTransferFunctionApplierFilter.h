#ifndef ORAIntensityTransferFunctionApplierFilter_h
#define ORAIntensityTransferFunctionApplierFilter_h
#include "ORAExport.h"
//ITK
#include <itkImageToImageFilter.h>
// internal
#include "oraIntensityTransferFunction.h"

namespace ora
{

/** @class IntensityTransferFunctionApplierFilter
 *
 * FIXME
 * - output image is always a float image
 * - 3D images
 *
 * @see itk::ImageToImageFilter
 *
 * @author phil
 * @version 1.0
 *
 * @ingroup ImageFilters
 **/
template<class TInputImage, class TOutputImage>
class  IntensityTransferFunctionApplierFilter:
	public itk::ImageToImageFilter<TInputImage, TOutputImage>
{
public:
  /** Standard class typedefs. */
  typedef IntensityTransferFunctionApplierFilter Self;
  typedef itk::ImageToImageFilter<TInputImage, TOutputImage > Superclass;
  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Accessibility typedefs. **/
  typedef TInputImage InputImageType;
  typedef typename InputImageType::Pointer InputImagePointer;
  typedef typename InputImageType::ConstPointer InputImageConstPointer;
  typedef typename InputImageType::PixelType InputPixelType;
  typedef TOutputImage OutputImageType;
  typedef typename OutputImageType::Pointer OutputImagePointer;
  typedef typename OutputImageType::ConstPointer OutputImageConstPointer;
  typedef typename OutputImageType::PixelType OutputPixelType;
  typedef typename OutputImageType::RegionType OutputImageRegionType;
  typedef ora::IntensityTransferFunction<InputPixelType, OutputPixelType> ITFType;
  typedef typename ITFType::Pointer ITFPointer;

  /** Run-time type information (and related methods). */
  itkTypeMacro(Self, Superclass)

  /** Method for creation through the object factory */
  itkNewMacro(Self)

  /** Set an optional intensity transfer function to be used during DRR
   * computation. A NULL pointer may or may not be tolerated by a concrete
   * DRR implementation. **/
  itkSetObjectMacro(ITF, ITFType)
  itkGetConstObjectMacro(ITF, ITFType)

protected:
  /** Set intensity transfer function to be used for mapping. **/
  ITFPointer m_ITF;

  /** Default constructor. **/
  IntensityTransferFunctionApplierFilter();
  /** Default destructor. **/
  virtual ~IntensityTransferFunctionApplierFilter();

  /** Print description of this object. **/
  virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;

  typedef itk::ThreadIdType ThreadIdType;

  /** Executes the ITF mapping.
   * @see GenerateData()
   * @see BeforeThreadedGenerateData()
   * @see AfterThreadedGenerateData() **/
  virtual void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);

private:
  /** Purposely not implemented. **/
  IntensityTransferFunctionApplierFilter(const Self&);
  /** Purposely not implemented. **/
  void operator=(const Self&);

};

}

#include "oraIntensityTransferFunctionApplierFilter.hxx"

#endif // ORAIntensityTransferFunctionApplierFilter_h
