#ifndef ORARTKDRRFilter_hxx
#define ORARTKDRRFilter_hxx

#include "oraRTKDRRFilter.h"
// STL
#include <math.h>
// ORA
#include "oraProjectionGeometry.h"
// ITK
#include <itkImageRegionConstIterator.h>
#include <itkImageRegionIterator.h>

namespace ora
{

	template<class TInputPixelType, class TOutputPixelType>
	const double RTKDRRFilter<TInputPixelType, TOutputPixelType>::EPSILON = 0.00001;

	template<class TInputPixelType, class TOutputPixelType>
	RTKDRRFilter<TInputPixelType, TOutputPixelType>::RTKDRRFilter()
		: Superclass()
	{
		this->m_LastGPUPreProcessingTime = -1;
		this->m_LastGPUPostProcessingTime = -1;
		this->m_LastGPUPureProcessingTime = -1;
		this->m_PreProcessingClock = ClockType::New();
		this->m_PostProcessingClock = ClockType::New();
		this->m_PureProcessingClock = ClockType::New();
		this->m_GeometryTransform = TransformType::New();
		this->m_CurrentGeometry = GeometryType::New();
		this->m_ITFApplier = nullptr;
		this->m_OffTheFlyITFMapping = false;
		this->m_IncorporateInverseSquareLaw = false;
	}

	template<class TInputPixelType, class TOutputPixelType>
	RTKDRRFilter<TInputPixelType, TOutputPixelType>::~RTKDRRFilter()
	{
		this->SetInput(nullptr);
		this->m_PreProcessingClock = nullptr;
		this->m_PostProcessingClock = nullptr;
		this->m_PureProcessingClock = nullptr;
		this->m_CurrentGeometry = nullptr;
		if (this->m_ITFApplier)
		{
			this->RTKDRRFilter<TInputPixelType, TOutputPixelType>::SetITF(nullptr);
			this->SetInput(nullptr);
		}
		this->m_ITFApplier = nullptr;
	}

	template<class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::GenerateOutputInformation()
	{
		if (this->m_NumberOfIndependentOutputs <= 0)
			return;
		this->UpdateCurrentImagingGeometry(); // update current geometry
		 // fix image size and further props with real (not temp.) projection
		this->EnsureCurrentImageIsCorrect(this->GetProjectionGeometry(this->m_CurrentDRROutputIndex));
		// needs to be checked to be sure that currentGeomentry is valid
		// if there is no geometry for the current output, the currentGeometry is not valid
		if (!this->DRRCanBeComputed())
		{
			itkExceptionMacro(<< "ERROR: " << this->GetNameOfClass()
				<< " (DRR cannot be computed. Verify that a valid input is set and a ProjectionGeometry"
				" for the current output exists.)")
		}
	}

	template<class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::SetInput(InputImagePointer input)
	{
		if (this->GetInput() != input)
		{
			this->Superclass::SetInput(input);
			ComputeMappedVolume(); // if necessary
		}
	}

	template <class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::SetGeometry(int index, GeometryPointer geometry)
	{
		Superclass::SetProjectionGeometry(index, geometry);
		Superclass::EnsureCurrentImageIsCorrect(geometry);
	}

	template<class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::SetITF(ITFType *_arg)
	{
		if (this->GetITF() != _arg)
		{
			this->Superclass::SetITF(_arg);
			ComputeMappedVolume(); // if necessary
		}
	}

	template<class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::GenerateData()
	{
		this->m_LastGPUPreProcessingTime = this->m_PreProcessingClock->GetTimeInSeconds();

		// Internal Cuda projector
		auto CudaForwardProjector = ForwardProjectorType::New();
		CudaForwardProjector->InPlaceOff();

		// Set input image, if ITF is set set the mapped volume as input image
		if(this->m_OffTheFlyITFMapping && this->m_ITF)
		{
			auto MappedVolume = this->m_ITFApplier->GetOutput();
			CudaForwardProjector->SetInput(1, MappedVolume);
		}
		else
		{
			CudaForwardProjector->SetInput(1, this->GetInput());
		}
		
		// Set Detector image
		auto CudaProjectionStack = OutputImageType::New();
		typename OutputImageType::IndexType rtkidx;
		rtkidx[0] = 0;
		rtkidx[1] = 0;
		rtkidx[2] = 0;
		auto DetectorSize = this->m_CurrentGeometry->GetDetectorSize();
		typename OutputImageType::SizeType rtksize;
		rtksize[0] = DetectorSize[0];
		rtksize[1] = DetectorSize[1];
		rtksize[2] = 1;
		typename OutputImageType::RegionType region(rtkidx, rtksize);
		CudaProjectionStack->SetRegions(region);
		CudaProjectionStack->Allocate();
		auto DetectorSpacing = this->m_CurrentGeometry->GetDetectorPixelSpacing();
		typename OutputImageType::SpacingType rtkspacing;
		rtkspacing[0] = DetectorSpacing[0];
		rtkspacing[1] = DetectorSpacing[1];
		rtkspacing[2] = 1;
		CudaProjectionStack->SetSpacing(rtkspacing);
		CudaForwardProjector->SetInput(0, CudaProjectionStack);

		// Transform ora::ProjectionGeometry to rtk::ThreeDProjectionGeometry
		auto ORAProjectionGeometry = this->m_CurrentGeometry;
		auto RTKProjectionGeometry = RTKGeometryType::New();
		RTKGeometryType::PointType SourcePosition;
		RTKGeometryType::PointType DetectorOrigin;
		RTKGeometryType::VectorType RowVector;
		RTKGeometryType::VectorType ColumnVector;
		for (auto i = 0; i < 3; ++i)
		{
			SourcePosition[i] = ORAProjectionGeometry->GetSourcePosition()[i];
			DetectorOrigin[i] = ORAProjectionGeometry->GetDetectorOrigin()[i];
			RowVector[i]      = ORAProjectionGeometry->GetDetectorRowOrientation()[i];
			ColumnVector[i]   = ORAProjectionGeometry->GetDetectorColumnOrientation()[i];
		}

		// Set rtk::Geometry
		RTKProjectionGeometry->AddProjection(SourcePosition, DetectorOrigin, RowVector, ColumnVector);
		CudaForwardProjector->SetGeometry(RTKProjectionGeometry);
		
		try
		{
			CudaForwardProjector->Update();
		}
		catch(itk::ExceptionObject &EO)
		{
			std::cout << "Exception caught during computing DRR." << std::endl;
			EO.Print(std::cout);
		}
		// WriteOutputImage(CudaForwardProjector->GetOutput(), "GPU");

		// Change image information to update the origin and the direction of the output image
		auto ChangeInformationFilter = ChangeInformationFilterType::New();
		ChangeInformationFilter->SetInput(CudaForwardProjector->GetOutput());

		GeometryPointer geom = this->GetProjectionGeometry(this->m_CurrentDRROutputIndex);
		typename OutputImageType::PointType DRROrigin;
		for (auto i = 0; i < 3; ++i)
		{
			DRROrigin[i] = geom->GetDetectorOrigin()[i];
		}
		ChangeInformationFilter->SetOutputOrigin(DRROrigin);
		ChangeInformationFilter->ChangeOriginOn();
		typename OutputImageType::DirectionType DRRDirection;
		auto DRRRowDirection = geom->GetDetectorRowOrientation();
		auto DRRColumnDirection = geom->GetDetectorColumnOrientation();
		auto DRRNormalDirection = itk::CrossProduct(DRRRowDirection, DRRColumnDirection);

		for (auto i = 0; i < 3; ++i)
		{
			DRRDirection[i][0] = DRRRowDirection[i];
			DRRDirection[i][1] = DRRColumnDirection[i];
			DRRDirection[i][2] = DRRNormalDirection[i];
		}
		ChangeInformationFilter->SetOutputDirection(DRRDirection);
		ChangeInformationFilter->ChangeDirectionOn();
		ChangeInformationFilter->Update();

		// running the minipipeline for the cuda-based DRR image and cleanup
		this->GetOutput(this->m_CurrentDRROutputIndex)->Graft(ChangeInformationFilter->GetOutput());
		CudaForwardProjector = nullptr;
		CudaProjectionStack->GetCudaDataManager()->Free();
		CudaProjectionStack = nullptr;
	}

	template<class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::SetOffTheFlyITFMapping(bool flag)
	{
		if (flag != this->m_OffTheFlyITFMapping)
		{
			this->m_OffTheFlyITFMapping = flag;
			ComputeMappedVolume(); // if necessary
			this->Modified();
		}
	}

	template<class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::ComputeMappedVolume()
	{
		if (this->GetInput() && this->m_ITF && m_OffTheFlyITFMapping)
		{
			// NOTE: If the off-the-fly ITF mode is active, we rather map all the
			// intensities of the volume (in a temporary internal volume) instead of
			// mapping the intensities on the fly during DRR computation. The internal
			// precision is FLOAT in order to save on memory. This may induce minor
			// inaccuracies in the output intensities which can be observed between
			// on-the-fly and off-the-fly modes.

			if (!this->m_ITFApplier)
				this->m_ITFApplier = ITFApplierType::New();
			this->m_ITFApplier->SetInput(this->GetInput());
			this->m_ITFApplier->SetITF(this->m_ITF);
			this->m_ITFApplier->Update(); // exceptions forwarded!

		}
		else
		{
			if (this->m_ITFApplier) // save memory
			{
				this->m_ITFApplier->SetInput(NULL);
				this->m_ITFApplier->SetITF(NULL);
				this->m_ITFApplier = NULL;
			}
		}
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::DRRCanBeComputed() const
	{
		if (!this->m_Input) // need input
			return false;
		typename InputImageType::RegionType reg = this->m_Input->GetLargestPossibleRegion();
		if (reg.GetNumberOfPixels() < 1) // input is requested to have valid size
			return false;
		//check if Geometry is set for this output
		if (!this->GetProjectionGeometry(this->m_CurrentDRROutputIndex))
		{
			return false;
		}
		//check if Geometry is valid
		return this->GetProjectionGeometry(this->m_CurrentDRROutputIndex)->IsGeometryValid();
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsGPUBased() const
	{
		return true; // GPU-based
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsCPUBased() const
	{
		return false; // not CPU-based
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsCPUMultiThreaded() const
	{
		return false; // neither CPU-based nor multi-threaded
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsSupportingDRRMasks() const
	{
		return false; // not implemented
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsSupportingITFOnTheFly() const
	{
		return false; // ITF on the fly is not implemented
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsSupportingITFOffTheFly() const
	{
		return true; // 
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsSupportingRigidTransformation() const
	{
		return true;
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsSupportingAffineTransformation() const
	{
		return false;
	}

	template<class TInputPixelType, class TOutputPixelType>
	bool RTKDRRFilter<TInputPixelType, TOutputPixelType>::IsSupportingElasticTransformation() const
	{
		return false;
	}

	template<class TInputPixelType, class TOutputPixelType>
	void RTKDRRFilter<TInputPixelType, TOutputPixelType>::UpdateCurrentImagingGeometry()
	{
		// current desired geometry:
		GeometryPointer geom = this->GetProjectionGeometry(this->m_CurrentDRROutputIndex);
		if (!geom)
			return;

		// NOTE: Here, we assume some matrix-offset-transform-based transform type.
		// More precisely, even a rigid transform. In order to get GetInverse() safely
		// to work, we reinterpret the transform as matrix-offset-transform.
		if (!this->m_Transform)
			return;
		TransformType *transform = dynamic_cast<TransformType*> (this->m_Transform.GetPointer());
		if (!transform)
			return;

		this->m_GeometryTransform->SetIdentity();
		// correct the spatial transform (if set) by applying it inversely:
		if (transform)
			transform->GetInverse(this->m_GeometryTransform);

		// correct the input volume orientation by applying it inversely:
		// (NOTE: We have to rotate about the volume's corner, not around the
		// first voxel's center - this was a bug before!)
		typename InputImageType::PointType O = this->m_Input->GetOrigin();
		typename InputImageType::DirectionType vdir = this->m_Input->GetDirection();
		typename InputImageType::SpacingType s = this->m_Input->GetSpacing();
		VectorType vv;
		vv[0] = vdir[0][0]; // volume row direction
		vv[1] = vdir[1][0];
		vv[2] = vdir[2][0];
		O = O - vv * s[0] / 2.;
		vv[0] = vdir[0][1]; // volume column direction
		vv[1] = vdir[1][1];
		vv[2] = vdir[2][1];
		O = O - vv * s[1] / 2.;
		vv[0] = vdir[0][2]; // volume slicing direction
		vv[1] = vdir[1][2];
		vv[2] = vdir[2][2];
		O = O - vv * s[2] / 2.;
		TransformPointer orientTransform = TransformType::New();
		typedef typename TransformType::MatrixType MatrixType;
		MatrixType Rv = MatrixType(this->m_Input->GetDirection().GetInverse());
		orientTransform->SetCenter(O);
		orientTransform->SetMatrix(Rv);
		// compose
		this->m_GeometryTransform->Compose(orientTransform, false);

		PointType sourcePosition;
		PointType DRROrigin;
		VectorType DRRRowDirection;
		VectorType DRRColumnDirection;
		for (auto i = 0; i < 3; i++)
		{
			sourcePosition[i] = geom->GetSourcePosition()[i];
			DRROrigin[i] = geom->GetDetectorOrigin()[i];
			DRRRowDirection[i] = geom->GetDetectorRowOrientation()[i];
			DRRColumnDirection[i] = geom->GetDetectorColumnOrientation()[i];
		}

		// inverse transform to account for rigid transform:
		sourcePosition = this->m_GeometryTransform->TransformPoint(sourcePosition);
		DRROrigin = this->m_GeometryTransform->TransformPoint(DRROrigin);
		DRRRowDirection = this->m_GeometryTransform->TransformVector(DRRRowDirection);
		DRRColumnDirection = this->m_GeometryTransform->TransformVector(DRRColumnDirection);

		// save back into currentGeometry
		double srcTemp[3];
		double drrOriginTemp[3];
		double drrRowDirTemp[3];
		double drrColumnDirTemp[3];
		for (auto i = 0; i < 3; i++)
		{
			srcTemp[i] = sourcePosition[i];
			drrOriginTemp[i] = DRROrigin[i];
			drrRowDirTemp[i] = DRRRowDirection[i];
			drrColumnDirTemp[i] = DRRColumnDirection[i];
		}
		this->m_CurrentGeometry->SetDetectorOrigin(drrOriginTemp);
		this->m_CurrentGeometry->SetSourcePosition(srcTemp);
		this->m_CurrentGeometry->SetDetectorRowOrientation(drrRowDirTemp);
		this->m_CurrentGeometry->SetDetectorColumnOrientation(drrColumnDirTemp);

		// these props are not affected, just copy them:
		this->m_CurrentGeometry->SetDetectorPixelSpacing(geom->GetDetectorPixelSpacing());
		this->m_CurrentGeometry->SetDetectorSize(geom->GetDetectorSize());
	}
}

#endif /* ORARTKDRRFilter_h */
