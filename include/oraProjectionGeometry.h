#ifndef ORAProjectionGeometry_h
#define ORAProjectionGeometry_h
#include "ORAExport.h"
//ITK
#include <itkObject.h>
#include <itkObjectFactory.h>

namespace ora
{

/** @class ProjectionGeometry
 * @brief Defines the nature of a perspective projection suitable for DRR computation.
 *
 * This class implements the basic definition of a perspective projection
 * geometry as proposed in "plastimatch digitally reconstructed radiographs
 * (DRR) application programming interface (API)" (design document).
 *
 * Please,
 * refer to this design document in order to retrieve more information on the
 * assumptions and restrictions regarding projection geometry definition!
 *
 * <b>Tests</b>:<br>
 * TestProjectionGeometry.cxx <br>
 *
 * @author phil
 * @author Markus Neuner
 * @version 1.2
 */
class ORA_EXPORT ProjectionGeometry : public itk::Object
{


public:
  /** Finite epsilon for floating point comparisons. **/
  static const double F_EPSILON;

  /** Standard class typedefs. **/
  typedef ProjectionGeometry Self;
  typedef itk::Object Superclass;
  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Method for creation through the object factory. **/
  itkNewMacro(Self)

  /** Run-time type information (and related methods). **/
  itkTypeMacro(ProjectionGeometry, itk::Object)

  /** Set the source position (idealized focal spot where the rays emerge from)
   * in physical units. **/
  void SetSourcePosition(const double position[3]);
  /** Set the source position (idealized focal spot where the rays emerge from)
   * in physical units.
   * @param positionX x coordinate of the source in WCS in physical units
   * @param positionY y coordinate of the source in WCS in physical units
   * @param positionZ z coordinate of the source in WCS in physical units
   */
  void SetSourcePosition(double positionX, double positionY, double positionZ)
  {
    double position[3];
    position[0] = positionX;
    position[1] = positionY;
    position[2] = positionZ;
    SetSourcePosition(position);
  }
  /** Get the source position (idealized focal spot where the rays emerge from)
   * in physical units. **/
  const double *GetSourcePosition() const;

  /** Set the detector origin (center of the first transmitted pixel position)
   * in physical units. **/
  void SetDetectorOrigin(const double origin[3]);
  /** Set the detector origin (center of the first transmitted pixel position)
   * in physical units.
   * @param originX x coordinate of the origin in WCS in physical units
   * @param originY y coordinate of the origin in WCS in physical units
   * @param originZ z coordinate of the origin in WCS in physical units
   */
  void SetDetectorOrigin(double originX, double originY, double originZ)
  {
    double origin[3];
    origin[0] = originX;
    origin[1] = originY;
    origin[2] = originZ;
    SetDetectorOrigin(origin);
  }
  /** Get the detector origin (center of the first transmitted pixel position)
   * in physical units. **/
  const double *GetDetectorOrigin() const;

  /** Get the detector center which is computed on the fly. */
  void GetDetectorCenter(double center[3]) const;

  /** Set the detector orientation (direction cosines of the "horizontal" row
   * vector and the "vertical" column vector). These vectors are internally
   * normalized if necessary. **/
  void SetDetectorOrientation(const double row[3], const double column[3]);
  /** Set a part of the detector orientation (direction cosines of the
   * "horizontal" row vector). This vector is internally
   * normalized if necessary. **/
  void SetDetectorRowOrientation(const double row[3]);
  /** Set a part of the detector orientation (direction cosines of the
   * "vertical" column vector). This vector is internally
   * normalized if necessary. **/
  void SetDetectorColumnOrientation(const double column[3]);
  /** Get a part of the detector orientation (direction cosines of the
   * "horizontal" row vector). Normalized vector. **/
  const double *GetDetectorRowOrientation() const;
  /** Get a part of the detector orientation (direction cosines of the
   * "vertical" column vector). Normalized vector. **/
  const double *GetDetectorColumnOrientation() const;

  /** Set the detector pixel spacing (distance from one pixel to another)
   * along row and column direction in physical units.  **/
  void SetDetectorPixelSpacing(const double spacing[2]);
  /** Set the detector pixel spacing (distance from one pixel to another)
   * along row and column direction in physical units.
   * @param rowSpacing spacing of a row pixels in physical units
   * @param columnSpacing spacing of a column pixels in physical units
   */
  void SetDetectorPixelSpacing(double rowSpacing, double columnSpacing)
  {
    double spacing[2];
    spacing[0] = rowSpacing;
    spacing[1] = columnSpacing;
    SetDetectorPixelSpacing(spacing);
  }
  /** Get the detector pixel spacing (distance from one pixel to another)
   * along row and column direction in physical units.  **/
  const double *GetDetectorPixelSpacing() const;

  /** Set the detector size along row and column direction in pixels.  **/
  void SetDetectorSize(const int size[2]);
  /** Set the detector size along row and column direction in pixels.
   * @param rowSize size of a row in pixels
   * @param columnSize size of a column in pixels
   */
  void SetDetectorSize(int rowSize, int columnSize)
  {
    int size[2];
    size[0] = rowSize;
    size[1] = columnSize;
    SetDetectorSize(size);
  }
  /** Get the detector size along row and column direction in pixels.  **/
  const int *GetDetectorSize() const;

  /** @return TRUE if the geometry is basically valid (applicable in terms of
   * DRR computation);<br>
   * 1. row and column vector must be orthogonal<br>
   * 2. detector size and spacing must be greater than 0<br>
   * 3. source position must not lie within the detector plane **/
  virtual bool IsGeometryValid() const;

  /** Computes and returns the homogeneous 3x4-projection-matrix from the
   * currently configured projection geometry settings. The returned flat
   * array contains the matrix components where the column moves fastest. **/
  virtual double *Compute3x4ProjectionMatrix() const;

  /** Set custom tolerance for dot product check (geometry validity) of
   * detector row / column orientation. Is set to F_EPSILON by default. */
  void SetDotProductTolerance(double tolerance)
  {
    m_DotProductTolerance = tolerance;
  }
  double GetDotProductTolerance()
  {
    return m_DotProductTolerance;
  }

  /** Computes and returns a specified corner point of the virtual detector. The
   * corner points relate to the pixel edges unlike the detector origin which
   * relates to the center of the first transmitted pixel.
   * @param index specifies which of the 4 corners to compute and return: 0 ...
   * left lower, 1 ... right lower, 2 ... right upper, 3 ... left upper
   * @param corner an array with at least 3 components which receives the requested
   * corner point's 3D coordinates if the computation succeeds
   * @return TRUE if the requested corner could be successfully computed, FALSE
   * otherwise (e.g. if current geometry is invalid or the specified index is
   * invalid)
   * @see IsGeometryValid()
   */
  bool GetCorner(int index, double *corner);

  /** Computes and returns the normal of the detector.
   * @param normal returned, normalized detector normal */
  void GetDetectorNormal(double *normal);

protected:
  /** Source position (idealized focal spot where the rays emerge from)
   * in physical units. **/
  double m_SourcePosition[3];
  /** Detector origin (center of the first transmitted pixel position)
   * in physical units. **/
  double m_DetectorOrigin[3];
  /** A part of the detector orientation (direction cosines of the
   * "horizontal" row vector). **/
  double m_DetectorRowOrientation[3];
  /** A part of the detector orientation (direction cosines of the
   * "vertical" column vector). **/
  double m_DetectorColumnOrientation[3];
  /** Detector pixel spacing (distance from one pixel to another)
   * along row and column direction in physical units.  **/
  double m_DetectorSpacing[2];
  /** Detector size along row and column direction in pixels.  **/
  int m_DetectorSize[2];
  /** Update helper. **/
  mutable unsigned long int m_LastMatrixTimeStamp;
  /** Homogeneous 3x4-projection-matrix according to the currently configured
   * projection geometry settings. **/
  mutable double m_ProjectionMatrix[12];
  /** Update helper. **/
  mutable unsigned long int m_LastValidationTimeStamp;
  /** Storage for validation. **/
  mutable bool m_GeometryValid;
  /** Configurable tolerance for dot product check of detector row / column
   * orientation **/
  double m_DotProductTolerance;

  /** Default constructor. **/
  ProjectionGeometry();
  /** Default constructor. **/
  virtual ~ProjectionGeometry();
  /** Standard object output. **/
  virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;

private:
  /** Purposely not implemented. **/
  ProjectionGeometry(const Self&);
  /** Purposely not implemented. **/
  void operator=(const Self&);

};

}

#endif /* #define ORAProjectionGeometry_h */
