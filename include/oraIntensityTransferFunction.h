#ifndef ORAIntensityTransferFunction_h
#define ORAIntensityTransferFunction_h
#include "ORAExport.h"
//ITK
#include <itkObject.h>
#include <itkObjectFactory.h>
#include <vector>

namespace ora
{

/** Supporting point definition of a 1D function. **/
struct SupportingPointStruct
{
  double inValue;
  double outValue;
};

/** @class IntensityTransferFunction
 * @brief Defines a general intensity transfer function (ITF) suitable for DRR generation.
 *
 * This class implements the basic definition of an intensity transfer function
 * (ITF) as proposed in "plastimatch digitally reconstructed radiographs
 * (DRR) application programming interface (API)" (design document).
 *
 * Please,
 * refer to this design document in order to retrieve more information on the
 * assumptions and restrictions regarding ITF definition!
 *
 * <b>Tests</b>:<br>
 * TestIntensityTransferFunction.cxx <br>
 *
 * @author phil
 * @author jeanluc
 * @version 1.1
 */
template<typename TInputPixel = float, typename TOutputPixel = float>
class ITK_EXPORT IntensityTransferFunction : public itk::Object
{
public:

  /** Standard class typedefs. **/
  typedef IntensityTransferFunction		Self;
  typedef itk::Object					Superclass;
  typedef itk::SmartPointer<Self>		Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  typedef std::vector<SupportingPointStruct> Vector;

  /** Method for creation through the object factory. **/
  itkNewMacro(Self);

  /** Run-time type information (and related methods). **/
  itkTypeMacro(IntensityTransferFunction, itk::Object);

  // Manipulation methods for adding/removing/retrieving supporting points
  /** Add new supporting point **/
  void AddSupportingPoint(double in, double out);
  
  /** Return the current number of supporting points **/
  unsigned int GetNumberOfSupportingPoints() const;
  
  /** Remove the supporting point number i, i: 0..n-1**/
  void RemoveSupportingPoint(int i); // i is 0-based supporting point index
  
  /** Clear all supporting points**/
  void RemoveAllSupportingPoints();
  
  /** Return all input intensity thresholds **/
  const Vector *GetSupportingPoints() const;

  /**
   * Clears all supporting points and parses the given string
   * to supporting points according to the format:
   * <number-of-support-points> <in-point-1> <out-point-1> <in-point-2> <out-point-2>
   **/
  bool DesignByString(const std::string &s);
  
// Mapping methods which map in values to out values according to the supporting
// points and computing values between the supporting points using 
// linear interpolation
  TOutputPixel MapInValue(TInputPixel in);
  void MapInValue(TInputPixel in, TOutputPixel &out);
  void MapInValues(int count, TInputPixel *ins, TOutputPixel *outs);
  
  itkSetMacro(Extrapolation, bool)
  itkGetMacro(Extrapolation, bool)

protected:
  /** Supporting point vector. **/
  Vector m_SupportingPoints;
  /** Update helper. **/
  mutable unsigned long int m_LastSortTimeStamp;
  /** Flag indicating whether values outside the supporting point range are
   * clamped to zero (FALSE) or extrapolated (TRUE) according to the nearest
   * defined function segment. */
  bool m_Extrapolation;

  /** Default constructor. **/
  IntensityTransferFunction();
  /** Default destructor. **/
  virtual ~IntensityTransferFunction();
  /** Standard object output. **/
  void PrintSelf(std::ostream& os, itk::Indent indent) const override;
  /** Calculate output intensity for given input intensity. **/
  double GetOutputIntensity(double in);
	/** Implements a smaller than implementation for Supporting point structure. **/
  static bool SupportingPointsSmallerThan(const SupportingPointStruct &sp1,
		 const SupportingPointStruct &sp2);
};

}

#include "oraIntensityTransferFunction.hxx"

#endif /* ORAIntensityTransferFunction_h */
