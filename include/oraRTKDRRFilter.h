#ifndef ORARTKDRRFilter_h
#define ORARTKDRRFilter_h
#include "ORAExport.h"
// STL
#include <vector>
// ITK
#include <itkConceptChecking.h>
#include <itkMatrixOffsetTransformBase.h>
#include <itkMacro.h>
#include <itkRealTimeClock.h>
#include <itkVector.h>
#include <itkPoint.h>
#include <itkSize.h>
#include <itkFixedArray.h>
#include <itkChangeInformationImageFilter.h>
// ORA
#include "oraCudaDRRFilter.h"
#include "oraIntensityTransferFunctionApplierFilter.h"
// RTK
#include "rtkCudaForwardProjectionImageFilter.h"
#include "rtkThreeDCircularProjectionGeometry.h"


namespace ora
{
	/** @class RTKDDRFilter
	 * @brief {Implements computation of digitally reconstructed radiographs (DRRs)
	 * for the GPU using exact radiological path tracing (Siddon sampling)}
	 *
	 * Both images, the input image (volume) as well as the output image
	 * (DRR), of this filter are assumed to be represented as 3D images. The DRR
	 * is single-sliced.
	 *
	 * The filter is templated over the input pixel type and the output pixel type.
	 *
	 * So-called independent outputs are implemented. This means that the filter can
	 * manage more than one output. DRR-computation refers to a specific output
	 * while the others are unmodified. This can be used for 2D/3D-registration with
	 * multiple images where DRRs with different projection geometry settings are
	 * required.
	 *
	 * Moreover, this class is capable of defining DRR masks for each independent
	 * output. These optional masks define whether or not a DRR pixel should be
	 * computed (value greater than 0). This may especially be useful for stochastic
	 * metric evaluations where only a subset of the DRR pixels is really used.
	 *
	 * Additionally, this class supports intensity transfer functions so that volume
	 * intensities can be mapped to volume output intensities before they are summed up.
	 * The intensity transfer function can be modified on the fly. In addition, there
	 * is an off-the-fly ITF mode which implies that the ITF is applied to the
	 * whole volume (whenever the volume or the ITF are changed) instead of computing
	 * the ITF mapping for each voxel (on-the-fly). However, this implementation
	 * tolerates NULL-ITF-pointers (no ITF mapping) which means that the raw input
	 * volume intensities are summed up.
	 *
	 * @see ora::CudaDRRFilter
	 *
	 * @author kabelitz
	 * @version 0.5
	 *
	 * @ingroup CudaImageFilters
	 **/
	template<class TInputPixelType, class TOutputPixelType>
	class RTKDRRFilter :
	public ora::CudaDRRFilter<TInputPixelType, TOutputPixelType>
	{
	public:
		ITK_DISALLOW_COPY_AND_ASSIGN(RTKDRRFilter);

		/** Standard class typedefs. */
		using Self         = RTKDRRFilter;
		using Superclass   = ora::CudaDRRFilter<TInputPixelType, TOutputPixelType>;
		using Pointer      = itk::SmartPointer<Self>;
		using ConstPointer = itk::SmartPointer<const Self> ;

		/** Accessibility typedefs. */
		using PointType   = itk::Point<double, 3>;
		using VectorType  = itk::Vector<double, 3>;
		using SizeType    = itk::Size<2>;
		using Spacingtype = itk::FixedArray<double, 2>;

		using OutputImageType                = typename Superclass::OutputImageType;
		using OutputImageRegionType          = typename Superclass::OutputImageRegionType;
		using OutputImagePointer             = typename Superclass::OutputImagePointer;
		using OutputImagePixelType           = typename Superclass::OutputImagePixelType;
		using InputImageType                 = typename Superclass::InputImageType;
		using InputImagePixelType            = typename Superclass::InputImagePixelType;
		using InputImage                     = typename Superclass::InputImageType;
		using InputImagePointer              = typename Superclass::InputImagePointer;
		using GeometryType                   = typename Superclass::GeometryType;
		using RTKGeometryType                = rtk::ThreeDCircularProjectionGeometry;
		using RTKGeometryPointer             = RTKGeometryType::Pointer;
		using RTKGeometryConstPointer        = RTKGeometryType::ConstPointer;
		using GeometryPointer                = typename Superclass::GeometryPointer;
		using MaskPixelType                  = typename Superclass::MaskPixelType;
		using MaskImagePointer               = typename Superclass::MaskImagePointer;
		using ITFType                        = typename Superclass::ITFType;
		using ITFPointer                     = typename Superclass::ITFPointer;
		using TransformType                  = itk::MatrixOffsetTransformBase<double, itkGetStaticConstMacro(InputImageDimension), itkGetStaticConstMacro(InputImageDimension)>;
		using TransformPointer               = typename TransformType::Pointer;
		using ChangeInformationFilterType    = itk::ChangeInformationImageFilter<OutputImageType>;
		using ChangeInformationFilterPointer = typename ChangeInformationFilterType::Pointer;

		/** Algorithm typedefs. */
		using ForwardProjectorType = rtk::CudaForwardProjectionImageFilter<InputImageType,OutputImageType> ;
		using ForwardProjectorPointer = typename ForwardProjectorType::Pointer ;

		/** Internal floating point comparison accuracy **/
		static const double EPSILON;

		/**Run-time type information (and related methods). */
		itkTypeMacro(Self, Superclass);

		/** Method for creation through the object factory */
		itkNewMacro(Self);

		/** Set the input image (3D volume) based on ITK image data.
		 * @param input the ITK image data set to be used for DRR computation **/
		void SetInput(InputImagePointer input) override;

		/** Set the input image for the detector data.
		 * @param geometry the ORA projection geometry for creating the output image **/
		virtual void SetGeometry(int index, GeometryPointer geometry);

		/** Generate information describing the output data.
		* A DRR filter usually produces an image with a different size than its input
		* image.
		* @see itk::ProcessObject#GenerateOutputInformaton() **/
		void GenerateOutputInformation() override;

		/** @return TRUE if current settings are sufficient for computing a DRR **/
		bool DRRCanBeComputed() const override;
		/** @return TRUE (this implementation is purely GPU-based) **/
		bool IsGPUBased() const override;
		/** @return FALSE (this implementation is purely GPU-based) **/
		bool IsCPUBased() const override;
		/** @return FALSE (this implementation is neither CPU-based nor multi-threaded) **/
		bool IsCPUMultiThreaded() const override;
		/** @return TRUE (this implementation supports "on the fly" ITFs) **/
		bool IsSupportingITFOnTheFly() const override;
		/** @return TRUE (this implementation supports "off the fly" ITFs) **/
		bool IsSupportingITFOffTheFly() const override;
		/** @return TRUE (this implementation supports only rigid transforms) **/
		bool IsSupportingRigidTransformation() const override;
		/** @return FALSE (this implementation supports only rigid transforms) **/
		bool IsSupportingAffineTransformation() const override;
		/** @return FALSE (this implementation supports only rigid transforms) **/
		bool IsSupportingElasticTransformation() const override;

		/** @return TRUE (this implementation supports DRR masks; however, NULL is
		 * also allowed) **/
		bool IsSupportingDRRMasks() const override;

		itkGetMacro(LastGPUPreProcessingTime, double)
		itkGetMacro(LastGPUPostProcessingTime, double)
		itkGetMacro(LastGPUPureProcessingTime, double)

		virtual void SetOffTheFlyITFMapping(bool flag);
		itkGetMacro(OffTheFlyITFMapping, bool)
		itkBooleanMacro(OffTheFlyITFMapping)

		itkGetMacro(IncorporateInverseSquareLaw, bool)
		itkSetMacro(IncorporateInverseSquareLaw, bool)
		itkBooleanMacro(IncorporateInverseSquareLaw)

		/** Set the ITF and check necessity of off-the-fly mapping.
		 * @see ora::DRRFilter::SetITF() */
		void SetITF(ITFType *_arg) override;

	protected:
		typedef itk::RealTimeClock ClockType;
		typedef ClockType::Pointer ClockPointer;
		typedef ora::IntensityTransferFunctionApplierFilter<InputImageType, OutputImageType> ITFApplierType;
		typedef typename ITFApplierType::Pointer ITFApplierPointer;
		typedef float MappedVolumePixelType;

		/** Stores pre-processing time of last DRR computation in milliseconds. **/
		double m_LastGPUPreProcessingTime;
		/** Stores post-processing time of last DRR computation in milliseconds. **/
		double m_LastGPUPostProcessingTime;
		/** Stores pure processing time of last DRR computation in milliseconds. **/
		double m_LastGPUPureProcessingTime;
		/** Tool clock for time measurements. **/
		ClockPointer m_PreProcessingClock;
		/** Tool clock for time measurements. **/
		ClockPointer m_PostProcessingClock;
		/** Tool clock for time measurements. **/
		ClockPointer m_PureProcessingClock;
		/** Current geometry. **/
		GeometryPointer m_CurrentGeometry;
		/** Transformation describing the current geometry transformation. **/
		TransformPointer m_GeometryTransform;
		/** Enables ITF mapping of the whole volume input before calculating the DRR.
		 * \warning {Off-the-fly ITF mapping can increase memory demand
		 * significantly!} */
		bool m_OffTheFlyITFMapping;
		/** ITF applier filter for mapping the input off-the-fly. **/
		ITFApplierPointer m_ITFApplier;
		/** Incorporate the inverse-square law which accounts for the x-rays' loss of power
		 * density along their rays (proportional to the inverse of the square of distance
		 * from the emitting point source). */
		bool m_IncorporateInverseSquareLaw;

		/** Pointer to GPU-based forward projector from the RTK.
		 * Maybe overthink this design decision. It doesnt release any memory and might be a memory leak.
		 */
		//ForwardProjectorPointer m_ForwardProjector;

		/** Executes the DRR computation. This method must be overridden in concrete
		* subclasses that are GPU-threaded.
		* @see GenerateData()
		* @see oraCudaDRRFilter **/
		virtual void GenerateData();

		/** Update Geometry to be valid after input transformation **/
		virtual void UpdateCurrentImagingGeometry();
		/** Compute mapped volume if off-the-fly ITF mapping is activated,
		 * the input volume is set and a valid ITF is set.
		 * \warning {Off-the-fly ITF mapping can increase memory demand
		 * significantly!} */
		virtual void ComputeMappedVolume();

		/** Default constructor. **/
		RTKDRRFilter();
		/** Default destructor. **/
		virtual ~RTKDRRFilter();

	};
}

#include "oraRTKDRRFilter.hxx"

#endif /* ORARTKDRRFilter_h */

