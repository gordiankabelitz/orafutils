ITKORA
=================================

.. |CircleCI| image:: https://circleci.com/gh/InsightSoftwareConsortium/ORA.svg?style=shield
    :target: https://circleci.com/gh/InsightSoftwareConsortium/ITKORA

.. |TravisCI| image:: https://travis-ci.org/InsightSoftwareConsortium/ORA.svg?branch=master
    :target: https://travis-ci.org/InsightSoftwareConsortium/ITKORA

.. |AppVeyor| image:: https://img.shields.io/appveyor/ci/kabcode/itkora.svg
    :target: https://ci.appveyor.com/project/kabcode/itkora

=========== =========== ===========
   Linux      macOS       Windows
=========== =========== ===========
|CircleCI|  |TravisCI|  |AppVeyor|
=========== =========== ===========

ORA

Registration project for 2D3D registration
